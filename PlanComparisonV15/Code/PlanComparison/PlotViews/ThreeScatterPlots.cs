﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;
using OxyPlot.Axes;

namespace PlanComparison.PlotViews
{
    class ThreeScatterPlots : IPlotView
    {
        public void SetAxes(PlotModel plotModel, double xMin, double xMax, double yMin, double yMax, string xLabel, string[] yLabel, IEnumerable<string> xTickLabels)
        {
            // xaxis
            var xaxis = new LinearAxis
            {
                Title = xLabel,
                Minimum = xMin,
                Maximum = xMax,
                Key = xLabel,
                TitleFontSize = 18,
                FontSize = 16,
                TitleFontWeight = FontWeights.Bold,
                AxisTitleDistance = 15,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Bottom

            };
            plotModel.Axes.Add(xaxis);

            // Three y-axes
            var yAxis1 = new LinearAxis
            {
                Title = yLabel[0],
                TitleFontSize = 18,
                FontSize = 16,
                TitleFontWeight = FontWeights.Bold,
                AxisTitleDistance = 15,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Left,
                StartPosition = 0,
                EndPosition = 0.3,
                Key = yLabel[0],
                MaximumPadding = 0.1
            };

            plotModel.Axes.Add(yAxis1);

            var yAxis2 = new LinearAxis
            {
                Title = yLabel[1],
                TitleFontSize = 18,
                FontSize = 16,
                TitleFontWeight = FontWeights.Bold,
                AxisTitleDistance = 15,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Left,
                StartPosition = 0.35,
                EndPosition = 0.65,
                Key = yLabel[1],
                MaximumPadding = 0.1
            };

            plotModel.Axes.Add(yAxis2);

            var yAxis3 = new LinearAxis
            {
                Title = yLabel[2],
                TitleFontSize = 18,
                FontSize = 16,
                TitleFontWeight = FontWeights.Bold,
                AxisTitleDistance = 15,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Left,
                StartPosition = 0.7,
                EndPosition = 1,
                Key = yLabel[2],
                MaximumPadding = 0.1
            };

            plotModel.Axes.Add(yAxis3);
        }

        public void SetLegend(PlotModel plotModel, bool isLegendVisible, LegendPlacement placement, LegendPosition position, LegendOrientation orientation)
        {
            //legend
            plotModel.LegendPlacement = placement;
            plotModel.LegendBorder = OxyColors.Black;
            plotModel.LegendBackground = OxyColor.FromAColor(32, OxyColors.Black);
            plotModel.LegendPosition = position;
            plotModel.LegendOrientation = orientation;
            plotModel.IsLegendVisible = isLegendVisible;
        }

        public void SetTitle(PlotModel plotModel, string title)
        {
            // Title
            plotModel.Title = title;
        }
    }
}
