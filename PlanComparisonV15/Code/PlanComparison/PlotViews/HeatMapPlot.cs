﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;
using OxyPlot.Axes;
using VMS.TPS.Common.Model.Types;
using VMS.TPS.Common.Model.API;

namespace PlanComparison.PlotViews
{
    public class HeatMapPlot : IPlotView
    {
        public void SetAxes(PlotModel plotModel, double xMin, double xMax, double yMin, double yMax, string xLabel, string[] yLabel, IEnumerable<string> xTickLabels)
        {
            // x-axis
            var xaxis = new OxyPlot.Axes.LinearAxis
            {
                Title = xLabel,
                TitleFontSize = 18,
                TitleFontWeight = FontWeights.Bold,
                AxisTitleDistance = 15,
                FontSize = 16,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Bottom,
                Key = xLabel
            };

            // y-axis
            var yaxis = new OxyPlot.Axes.LinearAxis
            {
                Title = yLabel[0],
                TitleFontSize = 18,
                TitleFontWeight = FontWeights.Bold,
                AxisTitleDistance = 15,
                FontSize = 16,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Left,
                Key = yLabel[0]
            };

            var zaxis = new OxyPlot.Axes.LinearColorAxis
            {
                Title = " Dose  [Gy]",
                Position = AxisPosition.Top,
                Palette = OxyPalettes.Rainbow(256),
                Maximum = 80 // change if necessary
            };


            plotModel.Axes.Add(xaxis);
            plotModel.Axes.Add(yaxis);
            plotModel.Axes.Add(zaxis);
        }

        public void SetLegend(PlotModel plotModel, bool isLegendVisible, LegendPlacement placement, LegendPosition position, LegendOrientation orientation)
        {
            //legend
            plotModel.LegendPlacement = placement;
            plotModel.LegendBorder = OxyColors.Black;
            plotModel.LegendBackground = OxyColor.FromAColor(32, OxyColors.Black);
            plotModel.LegendPosition = position;
            plotModel.LegendOrientation = orientation;
            plotModel.IsLegendVisible = isLegendVisible;
        }

        public void SetTitle(PlotModel plotModel, string title)
        {
            plotModel.Title = title;
        }
    }
}
