﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using Microsoft.Win32;

namespace MetricsTestV15
{
    public class TestConformity
    {
        private readonly PlanSetup UnderCovPlan;
        private readonly PlanSetup OverCovPlan;
        private readonly PlanSetup MovedCovPlan;

        public TestConformity(PlanSetup overCovPlan, PlanSetup underCovPlan, PlanSetup movedPlan)
        {
            UnderCovPlan = underCovPlan;
            OverCovPlan = overCovPlan;
            MovedCovPlan = movedPlan;
        }

        public void PrintOverCovResults()
        {
            string path = GetSavePath();

            // Define structures
            Structure sph_3 = this.OverCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm");
            Structure sph_5 = this.OverCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm");
            Structure sph_7 = this.OverCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm");
            Structure lng = this.OverCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_lng");
            Structure irreg = this.OverCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_irreg");

            // write results to csv file
            using (StreamWriter sw = new StreamWriter(path))
            {
                // title line
                sw.WriteLine("Scenario;PITV;CI Lomax & Scheib;CN;geomC;DSC;CI distance;CDI");

                // Over-coverage 1

                // Define structures
                Structure sph_31 = this.OverCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm1");
                Structure sph_51 = this.OverCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm1");
                Structure sph_71 = this.OverCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm1");
                Structure lng1 = this.OverCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_lng1");
                Structure irreg1 = this.OverCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_irreg1");
                sw.WriteLine("OC 0.5cm;");

                // Sphere 3cm diameter

                sw.Write("Sph_3cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_3, sph_31).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.OverCovPlan, sph_3, sph_31).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.OverCovPlan, sph_3, sph_31).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.OverCovPlan, sph_3, sph_31).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.OverCovPlan, sph_3, sph_31).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_3, sph_31).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_3, sph_31).ToString() + ";");
                sw.WriteLine();

                // Sphere 5cm diameter

                sw.Write("Sph_5cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_5, sph_51).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.OverCovPlan, sph_5, sph_51).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.OverCovPlan, sph_5, sph_51).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.OverCovPlan, sph_5, sph_51).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.OverCovPlan, sph_5, sph_51).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_5, sph_51).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_5, sph_51).ToString() + ";");
                sw.WriteLine();

                // Sphere 7cm diameter

                sw.Write("Sph_7cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_7, sph_71).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.OverCovPlan, sph_7, sph_71).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.OverCovPlan, sph_7, sph_71).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.OverCovPlan, sph_7, sph_71).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.OverCovPlan, sph_7, sph_71).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_7, sph_71).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_7, sph_71).ToString() + ";");
                sw.WriteLine();

                // Oblong

                sw.Write("Lng;");
                sw.Write(ConformityMetrics.CI_PITV(lng, lng1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.OverCovPlan, lng, lng1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.OverCovPlan, lng, lng1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.OverCovPlan, lng, lng1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.OverCovPlan, lng, lng1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(lng, lng1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(lng, lng1).ToString() + ";");
                sw.WriteLine();

                // Irregular

                sw.Write("Irreg;");
                sw.Write(ConformityMetrics.CI_PITV(irreg, irreg1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.OverCovPlan, irreg, irreg1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.OverCovPlan, irreg, irreg1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.OverCovPlan, irreg, irreg1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.OverCovPlan, irreg, irreg1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(irreg, irreg1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(irreg, irreg1).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();

                // Over-coverage 2
                sw.WriteLine("OC 1cm;");

                // Define structures
                Structure sph_32 = this.OverCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm2");
                Structure sph_52 = this.OverCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm2");
                Structure sph_72 = this.OverCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm2");
                Structure lng2 = this.OverCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_lng2");
                Structure irreg2 = this.OverCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_irreg2");

                // Sphere 3cm diameter

                sw.Write("Sph_3cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_3, sph_32).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.OverCovPlan, sph_3, sph_32).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.OverCovPlan, sph_3, sph_32).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.OverCovPlan, sph_3, sph_32).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.OverCovPlan, sph_3, sph_32).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_3, sph_32).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_3, sph_32).ToString() + ";");
                sw.WriteLine();

                // Sphere 5cm diameter

                sw.Write("Sph_5cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_5, sph_52).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.OverCovPlan, sph_5, sph_52).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.OverCovPlan, sph_5, sph_52).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.OverCovPlan, sph_5, sph_52).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.OverCovPlan, sph_5, sph_52).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_5, sph_52).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_5, sph_52).ToString() + ";");
                sw.WriteLine();

                // Sphere 7cm diameter

                sw.Write("Sph_7cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_7, sph_72).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.OverCovPlan, sph_7, sph_72).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.OverCovPlan, sph_7, sph_72).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.OverCovPlan, sph_7, sph_72).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.OverCovPlan, sph_7, sph_72).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_7, sph_72).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_7, sph_72).ToString() + ";");
                sw.WriteLine();

                // Oblong

                sw.Write("Lng;");
                sw.Write(ConformityMetrics.CI_PITV(lng, lng2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.OverCovPlan, lng, lng2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.OverCovPlan, lng, lng2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.OverCovPlan, lng, lng2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.OverCovPlan, lng, lng2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(lng, lng2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(lng, lng2).ToString() + ";");
                sw.WriteLine();

                // Irregular

                sw.Write("Irreg;");
                sw.Write(ConformityMetrics.CI_PITV(irreg, irreg2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.OverCovPlan, irreg, irreg2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.OverCovPlan, irreg, irreg2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.OverCovPlan, irreg, irreg2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.OverCovPlan, irreg, irreg2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(irreg, irreg2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(irreg, irreg2).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();


                // Over-coverage 3
                sw.WriteLine("OC 1.5cm;");

                // Define structures
                Structure sph_33 = this.OverCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm3");
                Structure sph_53 = this.OverCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm3");
                Structure sph_73 = this.OverCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm3");
                Structure lng3 = this.OverCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_lng3");
                Structure irreg3 = this.OverCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_irreg3");

                // Sphere 3cm diameter

                sw.Write("Sph_3cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_3, sph_33).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.OverCovPlan, sph_3, sph_33).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.OverCovPlan, sph_3, sph_33).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.OverCovPlan, sph_3, sph_33).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.OverCovPlan, sph_3, sph_33).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_3, sph_33).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_3, sph_33).ToString() + ";");
                sw.WriteLine();

                // Sphere 5cm diameter

                sw.Write("Sph_5cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_5, sph_53).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.OverCovPlan, sph_5, sph_53).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.OverCovPlan, sph_5, sph_53).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.OverCovPlan, sph_5, sph_53).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.OverCovPlan, sph_5, sph_53).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_5, sph_53).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_5, sph_53).ToString() + ";");
                sw.WriteLine();

                // Sphere 7cm diameter

                sw.Write("Sph_7cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_7, sph_73).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.OverCovPlan, sph_7, sph_73).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.OverCovPlan, sph_7, sph_73).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.OverCovPlan, sph_7, sph_73).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.OverCovPlan, sph_7, sph_73).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_7, sph_73).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_7, sph_73).ToString() + ";");
                sw.WriteLine();

                // Oblong

                sw.Write("Lng;");
                sw.Write(ConformityMetrics.CI_PITV(lng, lng3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.OverCovPlan, lng, lng3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.OverCovPlan, lng, lng3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.OverCovPlan, lng, lng3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.OverCovPlan, lng, lng3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(lng, lng3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(lng, lng3).ToString() + ";");
                sw.WriteLine();

                // Irregular

                sw.Write("Irreg;");
                sw.Write(ConformityMetrics.CI_PITV(irreg, irreg3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.OverCovPlan, irreg, irreg3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.OverCovPlan, irreg, irreg3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.OverCovPlan, irreg, irreg3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.OverCovPlan, irreg, irreg3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(irreg, irreg3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(irreg, irreg3).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();
            }
        }

        public void PrintUnderCovResults()
        {
            string path = GetSavePath();

            // Define structures
            Structure sph_3 = this.UnderCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm");
            Structure sph_5 = this.UnderCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm");
            Structure sph_7 = this.UnderCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm");
            Structure lng = this.UnderCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_lng");
            Structure irreg = this.UnderCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_irreg");

            // write results to csv file
            using (StreamWriter sw = new StreamWriter(path))
            {
                // title line
                sw.WriteLine("Scenario;PITV;CI Lomax & Scheib;CN;geomC;DSC;CI distance;CDI");

                // Under-coverage 1

                // Define structures
                Structure sph_31 = this.UnderCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm1");
                Structure sph_51 = this.UnderCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm1");
                Structure sph_71 = this.UnderCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm1");
                Structure lng1 = this.UnderCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_lng1");
                Structure irreg1 = this.UnderCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_irreg1");
                sw.WriteLine("UC 0.3cm;");

                // Sphere 3cm diameter

                sw.Write("Sph_3cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_3, sph_31).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.UnderCovPlan, sph_3, sph_31).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.UnderCovPlan, sph_3, sph_31).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.UnderCovPlan, sph_3, sph_31).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.UnderCovPlan, sph_3, sph_31).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_3, sph_31).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_3, sph_31).ToString() + ";");
                sw.WriteLine();

                // Sphere 5cm diameter

                sw.Write("Sph_5cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_5, sph_51).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.UnderCovPlan, sph_5, sph_51).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.UnderCovPlan, sph_5, sph_51).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.UnderCovPlan, sph_5, sph_51).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.UnderCovPlan, sph_5, sph_51).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_5, sph_51).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_5, sph_51).ToString() + ";");
                sw.WriteLine();

                // Sphere 7cm diameter

                sw.Write("Sph_7cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_7, sph_71).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.UnderCovPlan, sph_7, sph_71).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.UnderCovPlan, sph_7, sph_71).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.UnderCovPlan, sph_7, sph_71).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.UnderCovPlan, sph_7, sph_71).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_7, sph_71).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_7, sph_71).ToString() + ";");
                sw.WriteLine();

                // Oblong

                sw.Write("Lng;");
                sw.Write(ConformityMetrics.CI_PITV(lng, lng1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.UnderCovPlan, lng, lng1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.UnderCovPlan, lng, lng1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.UnderCovPlan, lng, lng1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.UnderCovPlan, lng, lng1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(lng, lng1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(lng, lng1).ToString() + ";");
                sw.WriteLine();

                // Irregular

                sw.Write("Irreg;");
                sw.Write(ConformityMetrics.CI_PITV(irreg, irreg1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.UnderCovPlan, irreg, irreg1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.UnderCovPlan, irreg, irreg1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.UnderCovPlan, irreg, irreg1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.UnderCovPlan, irreg, irreg1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(irreg, irreg1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(irreg, irreg1).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();

                // Under-coverage 2
                sw.WriteLine("UC 0.4cm;");

                // Define structures
                Structure sph_32 = this.UnderCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm2");
                Structure sph_52 = this.UnderCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm2");
                Structure sph_72 = this.UnderCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm2");
                Structure lng2 = this.UnderCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_lng2");
                Structure irreg2 = this.UnderCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_irreg2");
                sw.WriteLine("OC 0.5cm;");

                // Sphere 3cm diameter

                sw.Write("Sph_3cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_3, sph_32).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.UnderCovPlan, sph_3, sph_32).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.UnderCovPlan, sph_3, sph_32).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.UnderCovPlan, sph_3, sph_32).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.UnderCovPlan, sph_3, sph_32).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_3, sph_32).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_3, sph_32).ToString() + ";");
                sw.WriteLine();

                // Sphere 5cm diameter

                sw.Write("Sph_5cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_5, sph_52).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.UnderCovPlan, sph_5, sph_52).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.UnderCovPlan, sph_5, sph_52).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.UnderCovPlan, sph_5, sph_52).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.UnderCovPlan, sph_5, sph_52).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_5, sph_52).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_5, sph_52).ToString() + ";");
                sw.WriteLine();

                // Sphere 7cm diameter

                sw.Write("Sph_7cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_7, sph_72).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.UnderCovPlan, sph_7, sph_72).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.UnderCovPlan, sph_7, sph_72).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.UnderCovPlan, sph_7, sph_72).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.UnderCovPlan, sph_7, sph_72).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_7, sph_72).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_7, sph_72).ToString() + ";");
                sw.WriteLine();

                // Oblong

                sw.Write("Lng;");
                sw.Write(ConformityMetrics.CI_PITV(lng, lng2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.UnderCovPlan, lng, lng2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.UnderCovPlan, lng, lng2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.UnderCovPlan, lng, lng2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.UnderCovPlan, lng, lng2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(lng, lng2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(lng, lng2).ToString() + ";");
                sw.WriteLine();

                // Irregular

                sw.Write("Irreg;");
                sw.Write(ConformityMetrics.CI_PITV(irreg, irreg2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.UnderCovPlan, irreg, irreg2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.UnderCovPlan, irreg, irreg2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.UnderCovPlan, irreg, irreg2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.UnderCovPlan, irreg, irreg2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(irreg, irreg2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(irreg, irreg2).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();


                // Under-coverage 3
                sw.WriteLine("UC 0.5cm;");

                // Define structures
                Structure sph_33 = this.UnderCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm3");
                Structure sph_53 = this.UnderCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm3");
                Structure sph_73 = this.UnderCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm3");
                Structure lng3 = this.UnderCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_lng3");
                Structure irreg3 = this.UnderCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_irreg3");
                sw.WriteLine("OC 0.5cm;");

                // Sphere 3cm diameter

                sw.Write("Sph_3cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_3, sph_33).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.UnderCovPlan, sph_3, sph_33).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.UnderCovPlan, sph_3, sph_33).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.UnderCovPlan, sph_3, sph_33).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.UnderCovPlan, sph_3, sph_33).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_3, sph_33).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_3, sph_33).ToString() + ";");
                sw.WriteLine();

                // Sphere 5cm diameter

                sw.Write("Sph_5cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_5, sph_53).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.UnderCovPlan, sph_5, sph_53).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.UnderCovPlan, sph_5, sph_53).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.UnderCovPlan, sph_5, sph_53).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.UnderCovPlan, sph_5, sph_53).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_5, sph_53).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_5, sph_53).ToString() + ";");
                sw.WriteLine();

                // Sphere 7cm diameter

                sw.Write("Sph_7cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_7, sph_73).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.UnderCovPlan, sph_7, sph_73).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.UnderCovPlan, sph_7, sph_73).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.UnderCovPlan, sph_7, sph_73).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.UnderCovPlan, sph_7, sph_73).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_7, sph_73).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_7, sph_73).ToString() + ";");
                sw.WriteLine();

                // Oblong

                sw.Write("Lng;");
                sw.Write(ConformityMetrics.CI_PITV(lng, lng3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.UnderCovPlan, lng, lng3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.UnderCovPlan, lng, lng3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.UnderCovPlan, lng, lng3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.UnderCovPlan, lng, lng3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(lng, lng3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(lng, lng3).ToString() + ";");
                sw.WriteLine();

                // Irregular

                sw.Write("Irreg;");
                sw.Write(ConformityMetrics.CI_PITV(irreg, irreg3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.UnderCovPlan, irreg, irreg3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.UnderCovPlan, irreg, irreg3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.UnderCovPlan, irreg, irreg3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.UnderCovPlan, irreg, irreg3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(irreg, irreg3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(irreg, irreg3).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();
            }
        }

        public void PrintMovedCovResults()
        {
            string path = GetSavePath();

            // Define structures
            Structure sph_3 = this.MovedCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm");
            Structure sph_5 = this.MovedCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm");
            Structure sph_7 = this.MovedCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm");
            Structure lng = this.MovedCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_lng");
            Structure irreg = this.MovedCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_irreg");

            // write results to csv file
            using (StreamWriter sw = new StreamWriter(path))
            {
                // title line
                sw.WriteLine("Scenario;PITV;CI Lomax & Scheib;CN;geomC;DSC;CI distance;CDI");

                // Moved coverage

                // Define structures
                Structure sph_31 = this.MovedCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm1");
                Structure sph_51 = this.MovedCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm1");
                Structure sph_71 = this.MovedCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm1");
                Structure lng1 = this.MovedCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_lng1");
                Structure irreg1 = this.MovedCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_irreg1");
                sw.WriteLine("Moved 0.5cm;");

                // Sphere 3cm diameter

                sw.Write("Sph_3cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_3, sph_31).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.MovedCovPlan, sph_3, sph_31).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.MovedCovPlan, sph_3, sph_31).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.MovedCovPlan, sph_3, sph_31).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.MovedCovPlan, sph_3, sph_31).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_3, sph_31).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_3, sph_31).ToString() + ";");
                sw.WriteLine();

                // Sphere 5cm diameter

                sw.Write("Sph_5cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_5, sph_51).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.MovedCovPlan, sph_5, sph_51).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.MovedCovPlan, sph_5, sph_51).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.MovedCovPlan, sph_5, sph_51).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.MovedCovPlan, sph_5, sph_51).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_5, sph_51).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_5, sph_51).ToString() + ";");
                sw.WriteLine();

                // Sphere 7cm diameter

                sw.Write("Sph_7cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_7, sph_71).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.MovedCovPlan, sph_7, sph_71).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.MovedCovPlan, sph_7, sph_71).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.MovedCovPlan, sph_7, sph_71).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.MovedCovPlan, sph_7, sph_71).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_7, sph_71).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_7, sph_71).ToString() + ";");
                sw.WriteLine();

                // Oblong

                sw.Write("Lng;");
                sw.Write(ConformityMetrics.CI_PITV(lng, lng1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.MovedCovPlan, lng, lng1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.MovedCovPlan, lng, lng1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.MovedCovPlan, lng, lng1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.MovedCovPlan, lng, lng1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(lng, lng1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(lng, lng1).ToString() + ";");
                sw.WriteLine();

                // Irregular

                sw.Write("Irreg;");
                sw.Write(ConformityMetrics.CI_PITV(irreg, irreg1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.MovedCovPlan, irreg, irreg1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.MovedCovPlan, irreg, irreg1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.MovedCovPlan, irreg, irreg1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.MovedCovPlan, irreg, irreg1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(irreg, irreg1).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(irreg, irreg1).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();


                // Define structures
                Structure sph_32 = this.MovedCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm6");
                Structure sph_52 = this.MovedCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm6");
                Structure sph_72 = this.MovedCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm6");
                Structure lng2 = this.MovedCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_lng6");
                Structure irreg2 = this.MovedCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_irreg6");
                sw.WriteLine("Moved 1.0cm;");

                // Sphere 3cm diameter

                sw.Write("Sph_3cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_3, sph_32).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.MovedCovPlan, sph_3, sph_32).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.MovedCovPlan, sph_3, sph_32).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.MovedCovPlan, sph_3, sph_32).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.MovedCovPlan, sph_3, sph_32).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_3, sph_32).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_3, sph_32).ToString() + ";");
                sw.WriteLine();

                // Sphere 5cm diameter

                sw.Write("Sph_5cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_5, sph_52).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.MovedCovPlan, sph_5, sph_52).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.MovedCovPlan, sph_5, sph_52).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.MovedCovPlan, sph_5, sph_52).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.MovedCovPlan, sph_5, sph_52).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_5, sph_52).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_5, sph_52).ToString() + ";");
                sw.WriteLine();

                // Sphere 7cm diameter

                sw.Write("Sph_7cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_7, sph_72).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.MovedCovPlan, sph_7, sph_72).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.MovedCovPlan, sph_7, sph_72).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.MovedCovPlan, sph_7, sph_72).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.MovedCovPlan, sph_7, sph_72).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_7, sph_72).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_7, sph_72).ToString() + ";");
                sw.WriteLine();

                // Oblong

                sw.Write("Lng;");
                sw.Write(ConformityMetrics.CI_PITV(lng, lng2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.MovedCovPlan, lng, lng2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.MovedCovPlan, lng, lng2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.MovedCovPlan, lng, lng2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.MovedCovPlan, lng, lng2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(lng, lng2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(lng, lng2).ToString() + ";");
                sw.WriteLine();

                // Irregular

                sw.Write("Irreg;");
                sw.Write(ConformityMetrics.CI_PITV(irreg, irreg2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.MovedCovPlan, irreg, irreg2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.MovedCovPlan, irreg, irreg2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.MovedCovPlan, irreg, irreg2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.MovedCovPlan, irreg, irreg2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(irreg, irreg2).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(irreg, irreg2).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();


                // Define structures
                Structure sph_33 = this.MovedCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm7");
                Structure sph_53 = this.MovedCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm7");
                Structure sph_73 = this.MovedCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm7");
                Structure lng3 = this.MovedCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_lng7");
                Structure irreg3 = this.MovedCovPlan.StructureSet.Structures.First(s => s.Id == "PTV_irreg7");
                sw.WriteLine("Moved 1.5cm;");

                // Sphere 3cm diameter

                sw.Write("Sph_3cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_3, sph_33).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.MovedCovPlan, sph_3, sph_33).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.MovedCovPlan, sph_3, sph_33).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.MovedCovPlan, sph_3, sph_33).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.MovedCovPlan, sph_3, sph_33).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_3, sph_33).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_3, sph_33).ToString() + ";");
                sw.WriteLine();

                // Sphere 5cm diameter

                sw.Write("Sph_5cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_5, sph_53).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.MovedCovPlan, sph_5, sph_53).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.MovedCovPlan, sph_5, sph_53).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.MovedCovPlan, sph_5, sph_53).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.MovedCovPlan, sph_5, sph_53).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_5, sph_53).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_5, sph_53).ToString() + ";");
                sw.WriteLine();

                // Sphere 7cm diameter

                sw.Write("Sph_7cm;");
                sw.Write(ConformityMetrics.CI_PITV(sph_7, sph_73).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.MovedCovPlan, sph_7, sph_73).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.MovedCovPlan, sph_7, sph_73).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.MovedCovPlan, sph_7, sph_73).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.MovedCovPlan, sph_7, sph_73).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(sph_7, sph_73).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(sph_7, sph_73).ToString() + ";");
                sw.WriteLine();

                // Oblong

                sw.Write("Lng;");
                sw.Write(ConformityMetrics.CI_PITV(lng, lng3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.MovedCovPlan, lng, lng3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.MovedCovPlan, lng, lng3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.MovedCovPlan, lng, lng3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.MovedCovPlan, lng, lng3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(lng, lng3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(lng, lng3).ToString() + ";");
                sw.WriteLine();

                // Irregular

                sw.Write("Irreg;");
                sw.Write(ConformityMetrics.CI_PITV(irreg, irreg3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_LS(this.MovedCovPlan, irreg, irreg3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CN(this.MovedCovPlan, irreg, irreg3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_geomC(this.MovedCovPlan, irreg, irreg3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_DICE(this.MovedCovPlan, irreg, irreg3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_dist(irreg, irreg3).ToString() + ";");
                sw.Write(ConformityMetrics.CI_CDI(irreg, irreg3).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();
            }
        }


        // Prompt user to define path to save results
        private string GetSavePath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Choose csv file save location for conformity test",
                Filter = "csv files (*.csv)|*.csv"
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == true)
                return saveFileDialog.FileName;
            else
                return null;
        }
    }
}
