﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;

namespace MetricsTestV15
{
    public static class DoseMatrixMethods
    {
        /// <summary>
        /// Gets the voxelwise dose values from the dose object and returns them as a 3D array
        /// </summary>
        /// <param name="dose">Dose object</param>
        /// <param name="structure">Structure object</param>
        /// <returns>3D double array of voxel dose values. Voxels outside the structure are set to 0.</returns>
        public static double[,,] GetDoseThreeDFromIsodose(Structure isodose, Structure structure, Image image, double xres, double yres, double zres)
        {
            // number of voxels
            int xcount = (int)Math.Floor(image.XSize * image.XRes / xres);
            int ycount = (int)Math.Floor(image.YSize * image.YRes / yres);
            int zcount = (int)Math.Floor(image.ZSize * image.ZRes / zres);
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(xcount);
            System.Collections.BitArray isodoseSegmentStride = new System.Collections.BitArray(xcount);
            var doseMatrix = new double[xcount, ycount, zcount]; // 3D array with size = no of voxels

            for (int z = 0; z < zcount; z += 1)
            {
                for (int y = 0; y < ycount; y += 1)
                {
                    VVector start = image.Origin +
                                    image.YDirection * y * yres +
                                    image.ZDirection * z * zres;
                    VVector end = start + image.XDirection * xres * xcount;

                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);
                    SegmentProfile doseProfile = null;


                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            if (doseProfile == null)
                            {
                                doseProfile = isodose.GetSegmentProfile(start, end, isodoseSegmentStride);
                            }

                            doseMatrix[i, y, z] = Convert.ToDouble(doseProfile[i].Value);

                        }
                    }
                    doseProfile = null;
                }
            }
            return doseMatrix;
        }

        /// <summary>
        /// Gets the voxelwise dose values from the dose object and returns them as a 3D array
        /// </summary>
        /// <param name="dose">Dose object</param>
        /// <param name="structure">Structure object</param>
        /// <returns>3D double array of voxel dose values. Voxels outside the structure are set to 0.</returns>
        public static double[,,] GetMovedDoseThreeDFromIsodose(Structure isodose, Structure structure, Image image, VVector moveVector, double xres, double yres, double zres)
        {
            // number of voxels
            int xcount = (int)Math.Floor(image.XSize * image.XRes / xres);
            int ycount = (int)Math.Floor(image.YSize * image.YRes / yres);
            int zcount = (int)Math.Floor(image.ZSize * image.ZRes / zres);
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(xcount);
            System.Collections.BitArray isodoseSegmentStride = new System.Collections.BitArray(xcount);
            var doseMatrix = new double[xcount, ycount, zcount]; // 3D array with size = no of voxels

            for (int z = 0; z < zcount; z += 1)
            {
                for (int y = 0; y < ycount; y += 1)
                {
                    VVector start = image.Origin +
                                    image.YDirection * y * yres +
                                    image.ZDirection * z * zres;
                    VVector end = start + image.XDirection * xres * xcount;

                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);
                    SegmentProfile doseProfile = null;


                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            if (doseProfile == null)
                            {
                                doseProfile = isodose.GetSegmentProfile(new VVector(start.x - moveVector.x, start.y - moveVector.y, start.z - moveVector.z), new VVector(end.x - moveVector.x, end.y - moveVector.y, end.z - moveVector.z) , isodoseSegmentStride); // to simulate e.g. a shift of (1, 0, 0) the dose must be queried at (point - (1, 0, 0))
                            }

                            doseMatrix[i, y, z] = Convert.ToDouble(doseProfile[i].Value);

                        }
                    }
                    doseProfile = null;
                }
            }
            return doseMatrix;
        }

        /// <summary>
        /// Gets the voxelwise dose values from the dose object and returns them as a 1D array
        /// </summary>
        /// <param name="dose">Dose object</param>
        /// <param name="structure">Structure object</param>
        /// <returns>3D double array of voxel dose values. Voxels outside the structure are set to 0.</returns>
        public static double[] GetMovedDoseOneDFromIsodose(Structure isodose, Structure structure, Image image, VVector moveVector, double xres, double yres, double zres)
        {
            // number of voxels
            int xcount = (int)Math.Floor(image.XSize * image.XRes / xres);
            int ycount = (int)Math.Floor(image.YSize * image.YRes / yres);
            int zcount = (int)Math.Floor(image.ZSize * image.ZRes / zres);
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(xcount);
            System.Collections.BitArray isodoseSegmentStride = new System.Collections.BitArray(xcount);
            var doseMatrix = new List<double>(); // 1D array with size = no of voxels

            for (int z = 0; z < zcount; z += 1)
            {
                for (int y = 0; y < ycount; y += 1)
                {
                    VVector start = image.Origin +
                                    image.YDirection * y * yres +
                                    image.ZDirection * z * zres;
                    VVector end = start + image.XDirection * xres * xcount;

                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);
                    SegmentProfile doseProfile = null;


                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            if (doseProfile == null)
                            {
                                doseProfile = isodose.GetSegmentProfile(new VVector(start.x - moveVector.x, start.y - moveVector.y, start.z - moveVector.z), new VVector(end.x - moveVector.x, end.y - moveVector.y, end.z - moveVector.z), isodoseSegmentStride); // to simulate e.g. a shift of (1, 0, 0) the dose must be queried at (point - (1, 0, 0))
                            }

                            doseMatrix.Add(Convert.ToDouble(doseProfile[i].Value));
                        }
                        else
                        {
                            doseMatrix.Add(0);
                        }
                    }
                    doseProfile = null;
                }
            }
            return doseMatrix.ToArray();
        }

        public static double[,,] GetStructureDose3D(PlanSetup plan, Structure structure, double xres, double yres, double zres)
        {
            // number of voxels
            int xcount = (int)Math.Floor(plan.Dose.XSize * plan.Dose.XRes / xres);
            int ycount = (int)Math.Floor(plan.Dose.YSize * plan.Dose.YRes / yres);
            int zcount = (int)Math.Floor(plan.Dose.ZSize * plan.Dose.ZRes / zres);
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(xcount);
            double[] doseBuffer = new double[xcount];
            var doseMatrix = new double[xcount, ycount, zcount]; // 3D array with size = no of voxels

            for (int z = 0; z < zcount; z += 1)
            {
                for (int y = 0; y < ycount; y += 1)
                {
                    VVector start = plan.Dose.Origin +
                                    plan.Dose.YDirection * y * yres +
                                    plan.Dose.ZDirection * z * zres;
                    VVector end = start + plan.Dose.XDirection * xres * xcount;

                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);
                    DoseProfile doseProfile = null;


                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            if (doseProfile == null)
                            {
                                doseProfile = plan.Dose.GetDoseProfile(start, end, doseBuffer);
                            }

                            doseMatrix[i, y, z] = doseProfile[i].Value;

                        }
                    }
                    doseProfile = null;
                }
            }
            return doseMatrix;
        }

        public static double[,,] GetMovedStructureDose3D(PlanSetup plan, Structure structure, VVector moveVector, double xres, double yres, double zres)
        {
            // number of voxels
            int xcount = (int)Math.Floor(plan.Dose.XSize * plan.Dose.XRes / xres);
            int ycount = (int)Math.Floor(plan.Dose.YSize * plan.Dose.YRes / yres);
            int zcount = (int)Math.Floor(plan.Dose.ZSize * plan.Dose.ZRes / zres);
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(xcount);
            double[] doseBuffer = new double[xcount];
            var doseMatrix = new double[xcount, ycount, zcount]; // 3D array with size = no of voxels

            for (int z = 0; z < zcount; z += 1)
            {
                for (int y = 0; y < ycount; y += 1)
                {
                    VVector start = plan.Dose.Origin +
                                    plan.Dose.YDirection * y * yres +
                                    plan.Dose.ZDirection * z * zres;
                    VVector end = start + plan.Dose.XDirection * xres * xcount;

                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);
                    DoseProfile doseProfile = null;


                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            if (doseProfile == null)
                            {
                                doseProfile = plan.Dose.GetDoseProfile(new VVector(start.x - moveVector.x, start.y - moveVector.y, start.z - moveVector.z), new VVector(end.x - moveVector.x, end.y - moveVector.y, end.z - moveVector.z), doseBuffer); // to simulate e.g. a shift of (1, 0, 0) the dose must be queried at (point - (1, 0, 0));
                            }

                            doseMatrix[i, y, z] = doseProfile[i].Value;

                        }
                    }
                    doseProfile = null;
                }
            }
            return doseMatrix;
        }

        public static double[] GetMovedStructureDose1D(PlanSetup plan, Structure structure, VVector moveVector, double xres, double yres, double zres)
        {
            // number of voxels
            int xcount = (int)Math.Floor(plan.Dose.XSize * plan.Dose.XRes / xres);
            int ycount = (int)Math.Floor(plan.Dose.YSize * plan.Dose.YRes / yres);
            int zcount = (int)Math.Floor(plan.Dose.ZSize * plan.Dose.ZRes / zres);
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(xcount);
            double[] doseBuffer = new double[xcount];
            var doseMatrix = new List<double>(); // 1D array with size = no of voxels

            for (int z = 0; z < zcount; z += 1)
            {
                for (int y = 0; y < ycount; y += 1)
                {
                    VVector start = plan.Dose.Origin +
                                    plan.Dose.YDirection * y * yres +
                                    plan.Dose.ZDirection * z * zres;
                    VVector end = start + plan.Dose.XDirection * xres * xcount;

                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);
                    DoseProfile doseProfile = null;


                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            if (doseProfile == null)
                            {
                                doseProfile = plan.Dose.GetDoseProfile(new VVector(start.x - moveVector.x, start.y - moveVector.y, start.z - moveVector.z), new VVector(end.x - moveVector.x, end.y - moveVector.y, end.z - moveVector.z), doseBuffer); // to simulate e.g. a shift of (1, 0, 0) the dose must be queried at (point - (1, 0, 0));
                            }

                            doseMatrix.Add(doseProfile[i].Value);
                        }
                        else
                        {
                            doseMatrix.Add(0);
                        }
                    }
                    doseProfile = null;
                }
            }
            return doseMatrix.ToArray();
        }

        public static double[,,] GetBinaryStructureDoseHot3D(PlanSetup plan, Structure structure, double cutoffDose, double xres, double yres, double zres)
        {
            // number of voxels
            int xcount = (int)Math.Floor(plan.Dose.XSize * plan.Dose.XRes / xres);
            int ycount = (int)Math.Floor(plan.Dose.YSize * plan.Dose.YRes / yres);
            int zcount = (int)Math.Floor(plan.Dose.ZSize * plan.Dose.ZRes / zres);
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(xcount);
            double[] doseBuffer = new double[xcount];
            var doseMatrix = new double[xcount, ycount, zcount]; // 3D array with size = no of voxels

            for (int z = 0; z < zcount; z += 1)
            {
                for (int y = 0; y < ycount; y += 1)
                {
                    VVector start = plan.Dose.Origin +
                                    plan.Dose.YDirection * y * yres +
                                    plan.Dose.ZDirection * z * zres;
                    VVector end = start + plan.Dose.XDirection * xres * xcount;

                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);
                    DoseProfile doseProfile = null;


                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            if (doseProfile == null)
                            {
                                doseProfile = plan.Dose.GetDoseProfile(start, end, doseBuffer);
                            }

                            if (Convert.ToDouble(doseProfile[i].Value) >= cutoffDose)
                            {
                                doseMatrix[i, y, z] = 1;
                            }
                        }
                    }
                    doseProfile = null;
                }
            }
            return doseMatrix;
        }

        public static double[,,] GetBinaryStructureDoseCold3D(PlanSetup plan, Structure structure,  double cutoffDose, double xres, double yres, double zres)
        {
            // number of voxels
            int xcount = (int)Math.Floor(plan.Dose.XSize * plan.Dose.XRes / xres);
            int ycount = (int)Math.Floor(plan.Dose.YSize * plan.Dose.YRes / yres);
            int zcount = (int)Math.Floor(plan.Dose.ZSize * plan.Dose.ZRes / zres);
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(xcount);
            double[] doseBuffer = new double[xcount];
            var doseMatrix = new double[xcount, ycount, zcount]; // 3D array with size = no of voxels

            for (int z = 0; z < zcount; z += 1)
            {
                for (int y = 0; y < ycount; y += 1)
                {
                    VVector start = plan.Dose.Origin +
                                    plan.Dose.YDirection * y * yres +
                                    plan.Dose.ZDirection * z * zres;
                    VVector end = start + plan.Dose.XDirection * xres * xcount;

                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);
                    DoseProfile doseProfile = null;


                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            if (doseProfile == null)
                            {
                                doseProfile = plan.Dose.GetDoseProfile(start, end, doseBuffer);
                            }

                            if (Convert.ToDouble(doseProfile[i].Value) < cutoffDose)
                            {
                                doseMatrix[i, y, z] = 1;
                            }
                        }
                    }
                    doseProfile = null;
                }
            }
            return doseMatrix;
        }

        public static double[,,] GetBinaryMovedStructureDoseHot3D(PlanSetup plan, Structure structure, VVector moveVector, double cutoffDose, double xres, double yres, double zres)
        {
            // number of voxels
            int xcount = (int)Math.Floor(plan.Dose.XSize * plan.Dose.XRes / xres);
            int ycount = (int)Math.Floor(plan.Dose.YSize * plan.Dose.YRes / yres);
            int zcount = (int)Math.Floor(plan.Dose.ZSize * plan.Dose.ZRes / zres);
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(xcount);
            double[] doseBuffer = new double[xcount];
            var doseMatrix = new double[xcount, ycount, zcount]; // 3D array with size = no of voxels

            for (int z = 0; z < zcount; z += 1)
            {
                for (int y = 0; y < ycount; y += 1)
                {
                    VVector start = plan.Dose.Origin +
                                    plan.Dose.YDirection * y * yres +
                                    plan.Dose.ZDirection * z * zres;
                    VVector end = start + plan.Dose.XDirection * xres * xcount;

                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);
                    DoseProfile doseProfile = null;


                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            if (doseProfile == null)
                            {
                                doseProfile = plan.Dose.GetDoseProfile(new VVector(start.x - moveVector.x, start.y - moveVector.y, start.z - moveVector.z), new VVector(end.x - moveVector.x, end.y - moveVector.y, end.z - moveVector.z), doseBuffer); // to simulate e.g. a shift of (1, 0, 0) the dose must be queried at (point - (1, 0, 0));
                            }

                            if (doseProfile[i].Value >= cutoffDose)
                            {
                                doseMatrix[i, y, z] = 1;
                            }
                        }
                    }
                    doseProfile = null;
                }
            }
            return doseMatrix;
        }

        public static double[,,] GetBinaryMovedStructureDoseCold3D(PlanSetup plan, Structure structure, VVector moveVector, double cutoffDose, double xres, double yres, double zres)
        {
            // number of voxels
            int xcount = (int)Math.Floor(plan.Dose.XSize * plan.Dose.XRes / xres);
            int ycount = (int)Math.Floor(plan.Dose.YSize * plan.Dose.YRes / yres);
            int zcount = (int)Math.Floor(plan.Dose.ZSize * plan.Dose.ZRes / zres);
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(xcount);
            double[] doseBuffer = new double[xcount];
            var doseMatrix = new double[xcount, ycount, zcount]; // 3D array with size = no of voxels

            for (int z = 0; z < zcount; z += 1)
            {
                for (int y = 0; y < ycount; y += 1)
                {
                    VVector start = plan.Dose.Origin +
                                    plan.Dose.YDirection * y * yres +
                                    plan.Dose.ZDirection * z * zres;
                    VVector end = start + plan.Dose.XDirection * xres * xcount;

                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);
                    DoseProfile doseProfile = null;


                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            if (doseProfile == null)
                            {
                                doseProfile = plan.Dose.GetDoseProfile(new VVector(start.x - moveVector.x, start.y - moveVector.y, start.z - moveVector.z), new VVector(end.x - moveVector.x, end.y - moveVector.y, end.z - moveVector.z), doseBuffer); // to simulate e.g. a shift of (1, 0, 0) the dose must be queried at (point - (1, 0, 0));
                            }

                            if (doseProfile[i].Value < cutoffDose)
                            {
                                doseMatrix[i, y, z] = 1;
                            }
                        }

                    }
                    doseProfile = null;
                }
            }
            return doseMatrix;
        }

        public static double[] GetBinaryMovedStructureDoseHot1D(PlanSetup plan, Structure structure, VVector moveVector, double cutoffDose, double xres, double yres, double zres)
        {
            // number of voxels
            int xcount = (int)Math.Floor(plan.Dose.XSize * plan.Dose.XRes / xres);
            int ycount = (int)Math.Floor(plan.Dose.YSize * plan.Dose.YRes / yres);
            int zcount = (int)Math.Floor(plan.Dose.ZSize * plan.Dose.ZRes / zres);
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(xcount);
            double[] doseBuffer = new double[xcount];
            var doseMatrix = new List<double>(); // 1D array with size = no of voxels

            for (int z = 0; z < zcount; z += 1)
            {
                for (int y = 0; y < ycount; y += 1)
                {
                    VVector start = plan.Dose.Origin +
                                    plan.Dose.YDirection * y * yres +
                                    plan.Dose.ZDirection * z * zres;
                    VVector end = start + plan.Dose.XDirection * xres * xcount;

                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);
                    DoseProfile doseProfile = null;


                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            if (doseProfile == null)
                            {
                                doseProfile = plan.Dose.GetDoseProfile(new VVector(start.x - moveVector.x, start.y - moveVector.y, start.z - moveVector.z), new VVector(end.x - moveVector.x, end.y - moveVector.y, end.z - moveVector.z), doseBuffer); // to simulate e.g. a shift of (1, 0, 0) the dose must be queried at (point - (1, 0, 0));
                            }

                            if (doseProfile[i].Value >= cutoffDose)
                            {
                                doseMatrix.Add(1);
                            }
                            else
                            {
                                doseMatrix.Add(0);
                            }
                        }
                        else
                        {
                            doseMatrix.Add(0);
                        }
                    }
                    doseProfile = null;
                }
            }
            return doseMatrix.ToArray();
        }

        public static double[] GetBinaryMovedStructureDoseCold1D(PlanSetup plan, Structure structure, VVector moveVector, double cutoffDose, double xres, double yres, double zres)
        {
            // number of voxels
            int xcount = (int)Math.Floor(plan.Dose.XSize * plan.Dose.XRes / xres);
            int ycount = (int)Math.Floor(plan.Dose.YSize * plan.Dose.YRes / yres);
            int zcount = (int)Math.Floor(plan.Dose.ZSize * plan.Dose.ZRes / zres);
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(xcount);
            double[] doseBuffer = new double[xcount];
            var doseMatrix = new List<double>(); // 1D array with size = no of voxels

            for (int z = 0; z < zcount; z += 1)
            {
                for (int y = 0; y < ycount; y += 1)
                {
                    VVector start = plan.Dose.Origin +
                                    plan.Dose.YDirection * y * yres +
                                    plan.Dose.ZDirection * z * zres;
                    VVector end = start + plan.Dose.XDirection * xres * xcount;

                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);
                    DoseProfile doseProfile = null;


                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            if (doseProfile == null)
                            {
                                doseProfile = plan.Dose.GetDoseProfile(new VVector(start.x - moveVector.x, start.y - moveVector.y, start.z - moveVector.z), new VVector(end.x - moveVector.x, end.y - moveVector.y, end.z - moveVector.z), doseBuffer); // to simulate e.g. a shift of (1, 0, 0) the dose must be queried at (point - (1, 0, 0));
                            }

                            if (doseProfile[i].Value < cutoffDose)
                            {
                                doseMatrix.Add(1);
                            }
                            else
                            {
                                doseMatrix.Add(0);
                            }
                        }
                        else
                        {
                            doseMatrix.Add(0);
                        }
                    }
                    doseProfile = null;
                }
            }
            return doseMatrix.ToArray();
        }


        public static double[,,] GetStructure3D(Structure structure, Image image, double xres, double yres, double zres)
        {
            // number of voxels
            int xcount = (int)Math.Floor(image.XSize * image.XRes / xres);
            int ycount = (int)Math.Floor(image.YSize * image.YRes / yres);
            int zcount = (int)Math.Floor(image.ZSize * image.ZRes / zres);
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(xcount);
            var strMatrix = new double[xcount, ycount, zcount]; // 3D array with size = no of voxels

            for (int z = 0; z < zcount; z += 1)
            {
                for (int y = 0; y < ycount; y += 1)
                {
                    VVector start = image.Origin +
                                    image.YDirection * y * yres +
                                    image.ZDirection * z * zres;
                    VVector end = start + image.XDirection * xres * xcount;

                    SegmentProfile segmentProfile = structure.GetSegmentProfile(start, end, segmentStride);

                    for (int i = 0; i < segmentProfile.Count; i++)
                    {
                        if (segmentStride[i]) // if the point is in the structure
                        {
                            strMatrix[i, y, z] = Convert.ToDouble(segmentStride[i]);
                        }
                    }
                }
            }
            return strMatrix;
        }

        /// <summary>
        /// This method converts 1D arrays by filling in first x, then y, then z dimensions
        /// </summary>
        /// <param name="array"></param>
        /// <param name="xsize"></param>
        /// <param name="ysize"></param>
        /// <param name="zsize"></param>
        /// <returns></returns>
        public static double[,,] ConvertOneDToThreeD(double[] array, int xsize, int ysize, int zsize)
        {
            int count = 0;
            double[,,] retArray = new double[xsize, ysize, zsize];

            for (int z = 0; z < zsize; z++)
            {
                for (int y = 0; y < ysize; y++)
                {
                    for (int x = 0; x < xsize; x++)
                    {
                        retArray[x, y, z] = array[count];
                        count++;
                    }
                }
            }

            return retArray;
        }

        /// <summary>
        /// Method to create an EvaluationDose object given a 3D array of dose values.
        /// </summary>
        /// <param name="plan">PlanSetup to attach the EvaluationDose to</param>
        /// <param name="matrix">3D array of dose values in Gy</param>
        /// <param name="xResV">x resolution of the input dose array</param>
        /// <param name="yResV">y resolution of the input dose array</param>
        /// <param name="zResV">z resolution of the input dose array</param>
        /// <returns></returns>
        public static EvaluationDose MatrixToDose(ExternalPlanSetup plan, double[,,] matrix, double xResV, double yResV, double zResV)
        {
            Dose dose = plan.Dose;
            EvaluationDose retDose = plan.CreateEvaluationDose();

            // interpolate to original dose grid size
            var intDose = InterpolateDoseMatrix3D(matrix, xResV, yResV, zResV, dose);

            // set evaluation dose voxels
            for (int k = 0; k < dose.ZSize; k ++) 
            {
                int[,] voxels = new int[intDose.GetLength(0), intDose.GetLength(1)];

                for (int i = 0; i < intDose.GetLength(0); i++)
                {
                    for (int j = 0; j < intDose.GetLength(1); j++)
                    {
                        voxels[i, j] = retDose.DoseValueToVoxel(new DoseValue(intDose[i, j, k], DoseValue.DoseUnit.Gy));
                    }
                }

                retDose.SetVoxels(k, voxels);
            }

            return retDose;

            // Evaluation dose has the same properties and Methods as Dose. E.g.: 
            //Structure isodose = plan.StructureSet.AddStructure("CONTROL", "isodose");
            //isodose.ConvertDoseLevelToStructure(retDose, new DoseValue(20, DoseValue.DoseUnit.Gy));
        }


        public static double[,,] InterpolateDoseMatrix3D(double[,,] dose1, double xResV, double yResV, double zResV, Dose dose)
        {
            var interpMatrix = new double[dose.XSize, dose.YSize, dose.ZSize];

            // generate arrays to start from 
            double[] X = new double[dose1.GetLength(0)];
            for (int i = 0; i < dose1.GetLength(0); i++)
                X[i] = dose.Origin.x + (i * xResV * dose.XDirection.x);

            double[] Y = new double[dose1.GetLength(1)];
            for (int j = 0; j < dose1.GetLength(1); j++)
                Y[j] = dose.Origin.y + (j * yResV * dose.YDirection.y);

            double[] Z = new double[dose1.GetLength(2)];
            for (int k = 0; k < dose1.GetLength(1); k++)
                Z[k] = dose.Origin.z + (k * zResV * dose.ZDirection.z);

            var DoseOneD = new List<double>();

            double d;
            //alglib.spline2dinterpolant s;
            alglib.spline3dinterpolant s;

            for (int z = 0; z < dose1.GetLength(2); z++)
                for (int y = 0; y < dose1.GetLength(1); y++)
                    for (int x = 0; x < dose1.GetLength(0); x++)
                    {
                        DoseOneD.Add(dose1[x, y, z]);
                    }

            // initialize bilinear spline
            //alglib.spline2dbuildbilinearv(X, dose1.GetLength(0), Y, dose1.GetLength(1), DoseOneD.ToArray(), 1, out s);
            alglib.spline3dbuildtrilinearv(X, dose1.GetLength(0), Y, dose1.GetLength(1), Z, dose1.GetLength(2), DoseOneD.ToArray(), 1, out s);


            // get new dose matrix
            for (int i = 0; i < dose.XSize; i++)
                for (int j = 0; j < dose.YSize; j++)
                    for (int k = 0; k < dose.ZSize; k++)
                    {
                        d = alglib.spline3dcalc(s, dose.Origin.x + (i * dose.XRes * dose.XDirection.x), dose.Origin.y + (j * dose.YRes * dose.YDirection.y), dose.Origin.z + (k * dose.ZRes * dose.ZDirection.z));
                        interpMatrix[i, j, k] = d;
                    }


            return interpMatrix;
        }

    }
}
