﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using Microsoft.Win32;

namespace MetricsTestV15
{
    class TestDirGrad
    {
        // private fields
        private readonly PlanSetup AntDirPlan;
        private readonly PlanSetup RightDirPlan;

        // Constructor
        public TestDirGrad(PlanSetup antDirPlan, PlanSetup rightDirPlan)
        {
            this.AntDirPlan = antDirPlan;
            this.RightDirPlan = rightDirPlan;
        }

        public void PrintResultsDirection()
        {
            string path = GetSavePath();

            Structure sph7 = this.AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm");
            Structure sph7_iso = this.AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_iso");
            Structure sph7_ant = this.AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_ant0");
            Structure sph7_right = this.AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_right0");
            Structure sph7_oar = this.AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_OAR");

            Structure irreg = this.AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_irreg");
            Structure irreg_iso = this.AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_irreg_iso");
            Structure irreg_ant = this.AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_irreg_ant");
            Structure irreg_right = this.AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_irreg_right");
            Structure irreg_oar = this.AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_irreg_OAR");

            Structure OAR = this.AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "OAR");

            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine("Target shape;GI_SungChoi;GI_Dist_Anterior;GI_Dist_Right;GI_Dist_OAR;GI_Dose_Anterior;GI_Dose_Right;GI_Dose_OAR;GI_Mean_Dist_Anterior;GI_Mean_Dist_Right;GI_Mean_Dist_OAR;GI_Mean_Dose_Anterior;GI_Mean_Dose_Right;GI_Mean_Dose_OAR;GI_OAR_shortest;GI_OAR_avg");

                sw.WriteLine("Isotropic;");

                sw.Write("Sphere 7cm;");
                sw.Write(GradientMetrics.GI_SungChoi(sph7, sph7_iso).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_iso, OAR, "Anterior", 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_iso, OAR, "Right", 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_iso, OAR, "OAR", 2));
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_iso).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_iso).Item2.ToString());
                sw.Write(";");
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg_iso).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_iso, OAR, "Anterior", 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_iso, OAR, "Right", 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_iso, OAR, "OAR", 2));
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, irreg_iso).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, irreg_iso).Item2.ToString());
                sw.Write(";");
                sw.WriteLine();

                sw.WriteLine("Anterior bulge;");
                sw.Write("Sphere 7cm;");
                sw.Write(GradientMetrics.GI_SungChoi(sph7, sph7_ant).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_ant, OAR, "Anterior", 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_ant, OAR, "Right", 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_ant, OAR, "OAR", 2).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(this.AntDirPlan.Dose, sph7, OAR, "Anterior", 20, 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(this.AntDirPlan.Dose, sph7, OAR, "Right", 20, 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(this.AntDirPlan.Dose, sph7, OAR, "OAR", 20, 2).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.AntDirPlan, sph7, new VVector(0, -1, 0), new VVector(1, 0, 0), new VVector(0, 0, 1), sph7_ant));
                sw.Write(";");
                sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.AntDirPlan, sph7, new VVector(-1, 0, 0), new VVector(0, 1, 0), new VVector(0, 0, 1), sph7_ant));
                sw.Write(";");
                sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.AntDirPlan, sph7, OAR.CenterPoint - sph7.CenterPoint, sph7_ant).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.AntDirPlan, sph7, new VVector(0, -1, 0), new VVector(1, 0, 0), new VVector(0, 0, 1), 20));
                sw.Write(";");
                sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.AntDirPlan, sph7, new VVector(-1, 0, 0), new VVector(0, 1, 0), new VVector(0, 0, 1), 20));
                sw.Write(";");
                sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.AntDirPlan, sph7, OAR.CenterPoint - sph7.CenterPoint, 20).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_ant).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_ant).Item2.ToString());
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg_ant).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_ant, OAR, "Anterior", 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_ant, OAR, "Right", 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_ant, OAR, "OAR", 2));
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, irreg_ant).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, irreg_ant).Item2.ToString());
                sw.WriteLine();

                sw.WriteLine("Right bulge;");
                sw.Write("Sphere 7cm;");
                sw.Write(GradientMetrics.GI_SungChoi(sph7, sph7_right).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_right, OAR, "Anterior", 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_right, OAR, "Right", 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_right, OAR, "OAR", 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(this.RightDirPlan.Dose, sph7, OAR, "Anterior", 20, 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(this.RightDirPlan.Dose, sph7, OAR, "Right", 20, 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(this.RightDirPlan.Dose, sph7, OAR, "OAR", 20, 2).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.RightDirPlan, sph7, new VVector(0, -1, 0), new VVector(1, 0, 0), new VVector(0, 0, 1), sph7_right));
                sw.Write(";");
                sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.RightDirPlan, sph7, new VVector(-1, 0, 0), new VVector(0, 1, 0), new VVector(0, 0, 1), sph7_right));
                sw.Write(";");
                sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.RightDirPlan, sph7, OAR.CenterPoint - sph7.CenterPoint, sph7_right).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.RightDirPlan, sph7, new VVector(0, -1, 0), new VVector(1, 0, 0), new VVector(0, 0, 1), 20));
                sw.Write(";");
                sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.RightDirPlan, sph7, new VVector(-1, 0, 0), new VVector(0, 1, 0), new VVector(0, 0, 1), 20));
                sw.Write(";");
                sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.RightDirPlan, sph7, OAR.CenterPoint - sph7.CenterPoint, 20).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_right).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_right).Item2.ToString());
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg_right).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_right, OAR, "Anterior", 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_right, OAR, "Right", 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_right, OAR, "OAR", 2));
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, irreg_right).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, irreg_right).Item2.ToString());
                sw.WriteLine();

                sw.WriteLine("OAR gradient;");

                Structure sph7_iso0 = AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_iso0");
                Structure sph7_iso1 = AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_iso1");
                Structure sph7_iso2 = AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_iso2");
                Structure sph7_iso3 = AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_iso3");
                Structure sph7_iso4 = AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_iso4");

                Structure sph7_oar0 = AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_OAR0");
                Structure sph7_oar1 = AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_OAR1");
                Structure sph7_oar2 = AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_OAR2");
                Structure sph7_oar3 = AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_OAR3");
                Structure sph7_oar4 = AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_OAR4");

                sw.Write("Sphere 7cm steep towards OAR;");
                sw.Write(GradientMetrics.GI_SungChoi(sph7, sph7_oar0).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_oar0, OAR, "Anterior", 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_oar0, OAR, "Right", 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_oar0, OAR, "OAR", 2));
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_oar0).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_oar0).Item2.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_oar1).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_oar1).Item2.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_oar2).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_oar2).Item2.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_oar3).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_oar3).Item2.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_oar4).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_oar4).Item2.ToString());
                sw.WriteLine();

                sw.Write("Sphere 7cm isotropic;");
                sw.Write(GradientMetrics.GI_SungChoi(sph7, sph7_iso0).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_iso0, OAR, "Anterior", 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_iso0, OAR, "Right", 2));
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_iso0, OAR, "OAR", 2));
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_iso0).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_iso0).Item2.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_iso1).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_iso1).Item2.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_iso2).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_iso2).Item2.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_iso3).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_iso3).Item2.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_iso4).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_iso4).Item2.ToString());
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg_oar).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_oar, OAR, "Anterior", 2).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_oar, OAR, "Right", 2).ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindDirectionalGradient(irreg, irreg_oar, OAR, "OAR", 2).ToString());
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write("");
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, irreg_oar).Item1.ToString());
                sw.Write(";");
                sw.Write(GradientMetrics.FindOARGradient(OAR, irreg_oar).Item2.ToString());
                sw.WriteLine();

                // Get 2D anterior and Right gradient indices along CC axis - distance to dose
                List<Tuple<double, double>> antAntList = GradientMetrics.FindDirectionalGradientPerSlice(AntDirPlan, sph7, sph7_ant, "Anterior", 2);
                List<Tuple<double, double>> rightAntList = GradientMetrics.FindDirectionalGradientPerSlice(AntDirPlan, sph7, sph7_ant, "Right", 2);
                List<Tuple<double, double>> antRightList = GradientMetrics.FindDirectionalGradientPerSlice(AntDirPlan, sph7, sph7_right, "Anterior", 2);
                List<Tuple<double, double>> rightRightList = GradientMetrics.FindDirectionalGradientPerSlice(AntDirPlan, sph7, sph7_right, "Right", 2);

                // Get 2D anterior and Right gradient indices along CC axis - dose at distance
                List<Tuple<double, double>> antAntList1 = GradientMetrics.FindDirectionalGradientPerSlice(AntDirPlan, sph7, "Anterior", 20, 2);
                List<Tuple<double, double>> rightAntList1 = GradientMetrics.FindDirectionalGradientPerSlice(AntDirPlan, sph7, "Right", 20, 2);
                List<Tuple<double, double>> antRightList1 = GradientMetrics.FindDirectionalGradientPerSlice(RightDirPlan, sph7, "Anterior", 20, 2);
                List<Tuple<double, double>> rightRightList1 = GradientMetrics.FindDirectionalGradientPerSlice(RightDirPlan, sph7, "Right", 20, 2);

                sw.WriteLine("Distance to isodose");
                sw.WriteLine("Anterior bulge - anterior gradient");
                foreach (Tuple<double, double> point in antAntList)
                {
                    sw.WriteLine(string.Format("{0};{1}", point.Item1, point.Item2));
                }
                sw.WriteLine();

                sw.WriteLine("Anterior bulge - right gradient");
                foreach (Tuple<double, double> point in rightAntList)
                {
                    sw.WriteLine(string.Format("{0};{1}", point.Item1, point.Item2));
                }
                sw.WriteLine();

                sw.WriteLine("Right bulge - anterior gradient");
                foreach (Tuple<double, double> point in antRightList)
                {
                    sw.WriteLine(string.Format("{0};{1}", point.Item1, point.Item2));
                }
                sw.WriteLine();

                sw.WriteLine("Right bulge - Right gradient");
                foreach (Tuple<double, double> point in rightRightList)
                {
                    sw.WriteLine(string.Format("{0};{1}", point.Item1, point.Item2));
                }
                sw.WriteLine();

                sw.WriteLine("Dose at 20mm distance");
                sw.WriteLine("Anterior bulge - anterior gradient");
                foreach (Tuple<double, double> point in antAntList1)
                {
                    sw.WriteLine(string.Format("{0};{1}", point.Item1, point.Item2));
                }
                sw.WriteLine();

                sw.WriteLine("Anterior bulge - right gradient");
                foreach (Tuple<double, double> point in rightAntList1)
                {
                    sw.WriteLine(string.Format("{0};{1}", point.Item1, point.Item2));
                }
                sw.WriteLine();

                sw.WriteLine("Right bulge - anterior gradient");
                foreach (Tuple<double, double> point in antRightList1)
                {
                    sw.WriteLine(string.Format("{0};{1}", point.Item1, point.Item2));
                }
                sw.WriteLine();

                sw.WriteLine("Right bulge - Right gradient");
                foreach (Tuple<double, double> point in rightRightList1)
                {
                    sw.WriteLine(string.Format("{0};{1}", point.Item1, point.Item2));
                }
                sw.WriteLine();
            }
        }

        // This is probably very slow. I gave up and exported to MATLAB instead.
        public void DirectionRobustness(double shiftSize, int Nfx, int reps)
        {
            string path = GetSavePath();

            Structure sph7 = this.AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm");

            Structure sph7_ant = this.AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_ant0");
            Structure sph7_right = this.AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "PTV_sph7cm_right0");

            Structure OAR = this.AntDirPlan.StructureSet.Structures.FirstOrDefault(s => s.Id == "OAR");

            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine("Target shape;GI_SungChoi;GI_Dist_Anterior;GI_Dist_Right;GI_Dist_OAR;GI_Dose_Anterior;GI_Dose_Right;GI_Dose_OAR;GI_Mean_Dist_Anterior;GI_Mean_Dist_Right;GI_Mean_Dist_OAR;GI_Mean_Dose_Anterior;GI_Mean_Dose_Right;GI_Mean_Dose_OAR;GI_OAR_shortest;GI_OAR_avg");

                sw.WriteLine("Right bulge;");
                System.Windows.MessageBox.Show("Right bulge");
                sw.Write("Sphere 7cm;");
                for (int rep = 0; rep < reps; rep++)
                {
                    // Method to create accumulated total plan dose



                    sw.Write(GradientMetrics.GI_SungChoi(sph7, sph7_right).ToString());
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_right, OAR, "Anterior", 2));
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_right, OAR, "Right", 2));
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindDirectionalGradient(sph7, sph7_right, OAR, "OAR", 2));
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindDirectionalGradient(this.RightDirPlan.Dose, sph7, OAR, "Anterior", 20, 2));
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindDirectionalGradient(this.RightDirPlan.Dose, sph7, OAR, "Right", 20, 2));
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindDirectionalGradient(this.RightDirPlan.Dose, sph7, OAR, "OAR", 20, 2).ToString());
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.RightDirPlan, sph7, new VVector(0, -1, 0), new VVector(1, 0, 0), new VVector(0, 0, 1), sph7_right));
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.RightDirPlan, sph7, new VVector(-1, 0, 0), new VVector(0, 1, 0), new VVector(0, 0, 1), sph7_right));
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.RightDirPlan, sph7, OAR.CenterPoint - sph7.CenterPoint, sph7_right).ToString());
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.RightDirPlan, sph7, new VVector(0, -1, 0), new VVector(1, 0, 0), new VVector(0, 0, 1), 20));
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.RightDirPlan, sph7, new VVector(-1, 0, 0), new VVector(0, 1, 0), new VVector(0, 0, 1), 20));
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.RightDirPlan, sph7, OAR.CenterPoint - sph7.CenterPoint, 20).ToString());
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_right).Item1.ToString());
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindOARGradient(OAR, sph7_right).Item2.ToString());
                    sw.WriteLine();
                }

                sw.WriteLine("Anterior bulge;");
                System.Windows.MessageBox.Show("Anterior bulge");
                sw.Write("Sphere 7cm;");
                for (int rep = 0; rep < reps; rep++)
                {
                    sw.Write(GradientMetrics.GI_SungChoi(sph7, sph7_ant).ToString());
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindDirectionalGradient(this.AntDirPlan.Dose, sph7, OAR, "Anterior", 20, 2));
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindDirectionalGradient(this.AntDirPlan.Dose, sph7, OAR, "Right", 20, 2));
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindDirectionalGradient(this.AntDirPlan.Dose, sph7, OAR, "OAR", 20, 2).ToString());
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.AntDirPlan, sph7, new VVector(0, -1, 0), new VVector(1, 0, 0), new VVector(0, 0, 1), sph7_ant));
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.AntDirPlan, sph7, new VVector(-1, 0, 0), new VVector(0, 1, 0), new VVector(0, 0, 1), sph7_ant));
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.AntDirPlan, sph7, OAR.CenterPoint - sph7.CenterPoint, sph7_ant).ToString());
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.AntDirPlan, sph7, new VVector(0, -1, 0), new VVector(1, 0, 0), new VVector(0, 0, 1), 20));
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.AntDirPlan, sph7, new VVector(-1, 0, 0), new VVector(0, 1, 0), new VVector(0, 0, 1), 20));
                    sw.Write(";");
                    sw.Write(GradientMetrics.FindMeanDirectionalGradient(this.AntDirPlan, sph7, OAR.CenterPoint - sph7.CenterPoint, 20).ToString());
                    sw.WriteLine();
                }
            }
        }


        #region Helper methods
        // Prompt user to define path to save results
        private string GetSavePath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Choose csv file save location for gradient test",
                Filter = "csv files (*.csv)|*.csv"
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == true)
                return saveFileDialog.FileName;
            else
                return null;
        }
        #endregion
    }
}
