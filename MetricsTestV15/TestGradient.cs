﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using Microsoft.Win32;

namespace MetricsTestV15
{
    class TestGradient
    {
        private readonly PlanSetup Plan100;
        private readonly PlanSetup Plan75;
        private readonly PlanSetup Plan50;

        public TestGradient(PlanSetup plan100, PlanSetup plan75, PlanSetup plan50)
        {
            this.Plan100 = plan100;
            this.Plan75 = plan75;
            this.Plan50 = plan50;
        }

        public void PrintResults100()
        {
            string path = GetSavePath();

            // Define structures
            Structure sph_3 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm");
            Structure sph_5 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm");
            Structure sph_7 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm");
            Structure lng = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_lng");
            Structure irreg = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_irreg");

            // write results to csv file
            using (StreamWriter sw = new StreamWriter(path))
            {
                // title line
                sw.WriteLine("Scenario;GI Paddick;GI Wagner;GI Ohtakara;GI Mayo;GCI");

                // 1 cm to 50% isodose
                sw.WriteLine("1cm distance");
                Structure sph_31 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm+1cm");
                Structure sph_51 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm+1cm");
                Structure sph_71 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm+1cm");
                Structure lng1 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_lng+1cm");
                Structure irreg1 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_irreg+1cm");
                sw.WriteLine();

                sw.Write("Sphere3cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_3, sph_31).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere5cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_5, sph_51).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere7cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_7, sph_71).ToString() + ";");
                sw.WriteLine();

                sw.Write("Oblong;");
                sw.Write(GradientMetrics.GI_Paddick(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(lng, lng1).ToString() + ";");
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_Paddick(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg1).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();

                // 2 cm to 50% isodose
                sw.WriteLine("2cm distance");
                Structure sph_32 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm+2cm");
                Structure sph_52 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm+2cm");
                Structure sph_72 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm+2cm");
                Structure lng2 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_lng+2cm");
                Structure irreg2 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_irreg+2cm");
                sw.WriteLine();

                sw.Write("Sphere3cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_3, sph_32).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere5cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_5, sph_52).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere7cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_7, sph_72).ToString() + ";");
                sw.WriteLine();

                sw.Write("Oblong;");
                sw.Write(GradientMetrics.GI_Paddick(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(lng, lng2).ToString() + ";");
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_Paddick(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg2).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();

                // 3 cm to 50% isodose
                sw.WriteLine("3cm distance");
                Structure sph_33 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm+3cm");
                Structure sph_53 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm+3cm");
                Structure sph_73 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm+3cm");
                Structure lng3 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_lng+3cm");
                Structure irreg3 = this.Plan100.StructureSet.Structures.First(s => s.Id == "PTV_irreg+3cm");
                sw.WriteLine();

                sw.Write("Sphere3cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_3, sph_33).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere5cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_5, sph_53).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere7cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_7, sph_73).ToString() + ";");
                sw.WriteLine();

                sw.Write("Oblong;");
                sw.Write(GradientMetrics.GI_Paddick(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(lng, lng3).ToString() + ";");
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_Paddick(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg3).ToString() + ";");
                sw.WriteLine();
            }
        }

        public void PrintResults75()
        {
            string path = GetSavePath();

            // Define structures
            Structure sph_3 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm");
            Structure sph_5 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm");
            Structure sph_7 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm");
            Structure lng = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_lng");
            Structure irreg = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_irreg");

            // write results to csv file
            using (StreamWriter sw = new StreamWriter(path))
            {
                // title line
                sw.WriteLine("Scenario;GI Paddick;GI Wagner;GI Ohtakara;GI Mayo;GCI");

                // 1 cm to 50% isodose
                sw.WriteLine("1cm distance");
                Structure sph_31 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm+1cm");
                Structure sph_51 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm+1cm");
                Structure sph_71 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm+1cm");
                Structure lng1 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_lng+1cm");
                Structure irreg1 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_irreg+1cm");
                sw.WriteLine();

                sw.Write("Sphere3cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_3, sph_31).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere5cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_5, sph_51).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere7cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_7, sph_71).ToString() + ";");
                sw.WriteLine();

                sw.Write("Oblong;");
                sw.Write(GradientMetrics.GI_Paddick(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(lng, lng1).ToString() + ";");
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_Paddick(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg1).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();

                // 2 cm to 50% isodose
                sw.WriteLine("2cm distance");
                Structure sph_32 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm+2cm");
                Structure sph_52 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm+2cm");
                Structure sph_72 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm+2cm");
                Structure lng2 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_lng+2cm");
                Structure irreg2 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_irreg+2cm");
                sw.WriteLine();

                sw.Write("Sphere3cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_3, sph_32).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere5cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_5, sph_52).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere7cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_7, sph_72).ToString() + ";");
                sw.WriteLine();

                sw.Write("Oblong;");
                sw.Write(GradientMetrics.GI_Paddick(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(lng, lng2).ToString() + ";");
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_Paddick(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg2).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();

                // 3 cm to 50% isodose
                sw.WriteLine("3cm distance");
                Structure sph_33 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm+3cm");
                Structure sph_53 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm+3cm");
                Structure sph_73 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm+3cm");
                Structure lng3 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_lng+3cm");
                Structure irreg3 = this.Plan75.StructureSet.Structures.First(s => s.Id == "PTV_irreg+3cm");
                sw.WriteLine();

                sw.Write("Sphere3cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_3, sph_33).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere5cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_5, sph_53).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere7cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_7, sph_73).ToString() + ";");
                sw.WriteLine();

                sw.Write("Oblong;");
                sw.Write(GradientMetrics.GI_Paddick(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(lng, lng3).ToString() + ";");
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_Paddick(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg3).ToString() + ";");
                sw.WriteLine();
            }
        }

        public void PrintResults50()
        {
            string path = GetSavePath();

            // Define structures
            Structure sph_3 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm");
            Structure sph_5 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm");
            Structure sph_7 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm");
            Structure lng = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_lng");
            Structure irreg = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_irreg");

            // write results to csv file
            using (StreamWriter sw = new StreamWriter(path))
            {
                // title line
                sw.WriteLine("Scenario;GI Paddick;GI Wagner;GI Ohtakara;GI Mayo;GCI");

                // 1 cm to 50% isodose
                sw.WriteLine("1cm distance");
                Structure sph_31 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm+1cm");
                Structure sph_51 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm+1cm");
                Structure sph_71 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm+1cm");
                Structure lng1 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_lng+1cm");
                Structure irreg1 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_irreg+1cm");
                sw.WriteLine();

                sw.Write("Sphere3cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_3, sph_31).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_3, sph_31).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere5cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_5, sph_51).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_5, sph_51).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere7cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_7, sph_71).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_7, sph_71).ToString() + ";");
                sw.WriteLine();

                sw.Write("Oblong;");
                sw.Write(GradientMetrics.GI_Paddick(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(lng, lng1).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(lng, lng1).ToString() + ";");
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_Paddick(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(irreg, irreg1).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg1).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();

                // 2 cm to 50% isodose
                sw.WriteLine("2cm distance");
                Structure sph_32 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm+2cm");
                Structure sph_52 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm+2cm");
                Structure sph_72 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm+2cm");
                Structure lng2 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_lng+2cm");
                Structure irreg2 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_irreg+2cm");
                sw.WriteLine();

                sw.Write("Sphere3cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_3, sph_32).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_3, sph_32).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere5cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_5, sph_52).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_5, sph_52).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere7cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_7, sph_72).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_7, sph_72).ToString() + ";");
                sw.WriteLine();

                sw.Write("Oblong;");
                sw.Write(GradientMetrics.GI_Paddick(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(lng, lng2).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(lng, lng2).ToString() + ";");
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_Paddick(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(irreg, irreg2).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg2).ToString() + ";");
                sw.WriteLine();
                sw.WriteLine();

                // 3 cm to 50% isodose
                sw.WriteLine("3cm distance");
                Structure sph_33 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm+3cm");
                Structure sph_53 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm+3cm");
                Structure sph_73 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm+3cm");
                Structure lng3 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_lng+3cm");
                Structure irreg3 = this.Plan50.StructureSet.Structures.First(s => s.Id == "PTV_irreg+3cm");
                sw.WriteLine();

                sw.Write("Sphere3cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_3, sph_33).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_3, sph_33).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere5cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_5, sph_53).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_5, sph_53).ToString() + ";");
                sw.WriteLine();

                sw.Write("Sphere7cm;");
                sw.Write(GradientMetrics.GI_Paddick(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(sph_7, sph_73).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(sph_7, sph_73).ToString() + ";");
                sw.WriteLine();

                sw.Write("Oblong;");
                sw.Write(GradientMetrics.GI_Paddick(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(lng, lng3).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(lng, lng3).ToString() + ";");
                sw.WriteLine();

                sw.Write("Irregular;");
                sw.Write(GradientMetrics.GI_Paddick(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Wagner(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Ohtakara(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_Mayo(irreg, irreg3).ToString() + ";");
                sw.Write(GradientMetrics.GI_SungChoi(irreg, irreg3).ToString() + ";");
                sw.WriteLine();
            }
        }

      

        // Prompt user to define path to save results
        private string GetSavePath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Choose csv file save location for gradient test",
                Filter = "csv files (*.csv)|*.csv"
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == true)
                return saveFileDialog.FileName;
            else
                return null;
        }
    }
}
