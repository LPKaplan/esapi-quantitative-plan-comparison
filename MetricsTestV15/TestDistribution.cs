﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using Microsoft.Win32;
using ConnectedComponentLabeling;
using MathNet.Numerics.Distributions;

namespace MetricsTestV15
{
    class TestDistribution
    {
        private readonly PlanSetup Plan;
        private readonly Image image;

        public TestDistribution(PlanSetup plan, Image image)
        {
            this.Plan = plan;
            this.image = image;
        }

        public void PrintResults()
        {
            string path = GetSavePath();

            // write results to csv file
            using (StreamWriter sw = new StreamWriter(path))
            {
                double xres = 1;
                double yres = 1;
                double zres = 1;

                Structure PTV = Plan.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm");

                // title line
                sw.WriteLine("Scenario;LCS_D;Number of cold spots above 0.1cc;Cold spot sizes");

                // One cold spot
                sw.Write("One cold spot;");
                Structure Cold1 = Plan.StructureSet.Structures.First(s => s.Id == "PTV_cold");

                sw.Write(DistributionMetrics.CalculateLCSd(Plan, PTV, Cold1, image).ToString() + ";");

                var oneCSDoseMatrix = DoseMatrixMethods.GetDoseThreeDFromIsodose(Cold1, PTV, image, xres, yres, zres);
                var CSCCL = new BinaryCCL();
                var oneCS = CSCCL.ProcessReturnOnlySize(oneCSDoseMatrix, 66); // Cutoff value is arbitrary and not used


                sw.Write(oneCS.Count(cs => (cs.Value * xres * yres * zres / 1000) > 0.1).ToString() + ";");
                foreach (var cs in oneCS)
                {
                    sw.Write((cs.Value * xres * yres * zres / 1000).ToString() + ";");
                }
                sw.WriteLine();

                // Two cold spots
                sw.Write("Two cold spots;");
                Structure Cold2 = Plan.StructureSet.Structures.First(s => s.Id == "PTV_cold1");

                sw.Write(DistributionMetrics.CalculateLCSd(Plan, PTV, Cold2, image).ToString() + ";");

                var twoCSDoseMatrix = DoseMatrixMethods.GetDoseThreeDFromIsodose(Cold2, PTV, image, xres, yres, zres);
                var twoCS = CSCCL.ProcessReturnOnlySize(twoCSDoseMatrix, 66); // Cutoff value is arbitrary and not used
                sw.Write(twoCS.Count(cs => (cs.Value * xres * yres * zres / 1000) > 0.1).ToString() + ";");
                foreach (var cs in twoCS)
                {
                    sw.Write((cs.Value * xres * yres * zres / 1000).ToString() + ";");
                }
                sw.WriteLine();

                // Many cold spots
                sw.Write("Many cold spots;");
                Structure Cold3 = Plan.StructureSet.Structures.First(s => s.Id == "PTV_cold2");

                sw.Write(DistributionMetrics.CalculateLCSd(Plan, PTV, Cold3, image).ToString() + ";");

                var manyCSDoseMatrix = DoseMatrixMethods.GetDoseThreeDFromIsodose(Cold3, PTV, image, xres, yres, zres);
                var manyCS = CSCCL.ProcessReturnOnlySize(manyCSDoseMatrix, 66); // Cutoff value is arbitrary and not used
                sw.Write(manyCS.Count(cs => (cs.Value * xres * yres * zres / 1000) > 0.1).ToString() + ";");
                foreach (var cs in manyCS)
                {
                    sw.Write((cs.Value * xres * yres * zres / 1000).ToString() + ";");
                }
                sw.WriteLine();
            }
        }

        public void RobustDistribution(double shiftStd, int Nfx, int reps)
        {
            // Get save path
            string path = GetSavePath();

            // set dose matrix resolution
            double xres = 1;
            double yres = 1;
            double zres = 1;

            // Get PTV Structure
            Structure PTV = Plan.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm");

            // initialize random number generator
            Random random = new Random();  // generate number between 0 and 1: double n = random.NextDouble();
            Normal normalDist = new Normal(0, shiftStd);

            // initialize connected component labeling object
            var CSCCL = new BinaryCCL();

            // write results to csv file
            using (StreamWriter sw = new StreamWriter(path))
            {
                // title line
                sw.WriteLine("Scenario;LCS_D;Number of cold spots above 0.1cc;Cold spot sizes");

                // One cold spot
                sw.Write("One cold spot;");
                MessageBox.Show("One cold spot...");
                Structure Cold1 = Plan.StructureSet.Structures.First(s => s.Id == "PTV_cold");

                for (int rep = 0; rep < reps; rep++)
                {
                    MessageBox.Show(string.Format("Repetition number: {0}", rep));
                    // Get accumulated matrix over 30 fx with random shifts

                    // first fraction
                    // get random shift vectors
                    double xshift = random.NextDouble();
                    double yshift = random.NextDouble();
                    double zshift = random.NextDouble();
                    VVector shift = new VVector(xshift, yshift, zshift);
                    VVector shiftN = shift / shift.Length; // normalized shift
                    
                    double shiftSize = normalDist.Sample();

                    double[] cumMat = DoseMatrixMethods.GetMovedDoseOneDFromIsodose(Cold1, PTV, image, shiftSize*shiftN, xres, yres, zres); // accumulating dose matrices is faster in 1D. Convert these to 3D later.
                    
                    // next fractions
                    for (int fx = 1; fx < Nfx; fx++)
                    {
                        Console.WriteLine(string.Format("Fraction number: {0}", fx));

                        // get random shift vectors
                        xshift = random.NextDouble();
                        yshift = random.NextDouble();
                        zshift = random.NextDouble();
                        shift = new VVector(xshift, yshift, zshift);
                        shiftN = shift / shift.Length; // normalized shift
                        shiftSize = normalDist.Sample();

                        double[] temp = DoseMatrixMethods.GetMovedDoseOneDFromIsodose(Cold1, PTV, image, shiftSize*shiftN, xres, yres, zres);
                        for (int i = 0; i < cumMat.Length; i++)
                        {
                            cumMat[i] += temp[i];
                        }
                    }

                    // cumMat now contains the number of fractions in which a voxel is under-dosed. For simplicity, let us assume that a voxel is underdosed over the entire treatment course only if it is underdosed in at least five fractions
                    double[] binaryMat1D = new double[cumMat.Length];
                    for (int i = 0; i < cumMat.Length; i++)
                    {
                        if (cumMat[i] >= 5)
                        {
                            binaryMat1D[i] = 1;
                        }
                        else
                        {
                            binaryMat1D[i] = 0;
                        }
                    }

                    // convert to 3D matrix
                    // number of voxels
                    int xcount = (int)Math.Floor(image.XSize * image.XRes / xres);
                    int ycount = (int)Math.Floor(image.YSize * image.YRes / yres);
                    int zcount = (int)Math.Floor(image.ZSize * image.ZRes / zres);

                    MessageBox.Show("Converting 1D array to 3D...");
                    double[,,] binaryMat3D = DoseMatrixMethods.ConvertOneDToThreeD(binaryMat1D, xcount, ycount, zcount);

                    // LCSd
                    sw.Write(DistributionMetrics.CalculateLCSd(binaryMat3D).ToString() + ";");

                    // cold spot number and size
                    var oneCS = CSCCL.ProcessReturnOnlySize(binaryMat3D, 66); // Cutoff value is arbitrary and not used

                    sw.Write(oneCS.Count(cs => (cs.Value * xres * yres * zres / 1000) > 0.1).ToString() + ";");
                    foreach (var cs in oneCS)
                    {
                        sw.Write((cs.Value * xres * yres * zres / 1000).ToString() + ";");
                    }
                    sw.WriteLine();
                }

                // Two cold spots
                sw.Write("Two cold spots;");
                MessageBox.Show("Two cold spots...");
                Structure Cold2 = Plan.StructureSet.Structures.First(s => s.Id == "PTV_cold1");

                for (int rep = 0; rep < reps; rep++)
                {
                    // Get accumulated matrix over 30 fx with random shifts
                    MessageBox.Show(string.Format("Repetition number: {0}", rep));
                    // first fraction
                    // get random shift vectors
                    double xshift = random.NextDouble();
                    double yshift = random.NextDouble();
                    double zshift = random.NextDouble();
                    VVector shift = new VVector(xshift, yshift, zshift);
                    VVector shiftN = shift / shift.Length; // normalized shift

                    double shiftSize = normalDist.Sample();

                    double[] cumMat = DoseMatrixMethods.GetMovedDoseOneDFromIsodose(Cold2, PTV, image, shiftSize * shiftN, xres, yres, zres); // accumulating dose matrices is faster in 1D. Convert these to 3D later.

                    // next fractions
                    for (int fx = 1; fx < Nfx; fx++)
                    {
                        // get random shift vectors
                        xshift = random.NextDouble();
                        yshift = random.NextDouble();
                        zshift = random.NextDouble();
                        shift = new VVector(xshift, yshift, zshift);
                        shiftN = shift / shift.Length; // normalized shift

                        shiftSize = normalDist.Sample();

                        double[] temp = DoseMatrixMethods.GetMovedDoseOneDFromIsodose(Cold2, PTV, image, shiftSize * shiftN, xres, yres, zres);
                        for (int i = 0; i < cumMat.Length; i++)
                        {
                            cumMat[i] += temp[i];
                        }
                    }

                    // cumMat now contains the number of fractions in which a voxel is under-dosed. For simplicity, let us assume that a voxel is underdosed over the entire treatment course only if it is underdosed in at least five fractions
                    double[] binaryMat1D = new double[cumMat.Length];
                    for (int i = 0; i < cumMat.Length; i++)
                    {
                        if (cumMat[i] >= 5)
                        {
                            binaryMat1D[i] = 1;
                        }
                        else
                        {
                            binaryMat1D[i] = 0;
                        }
                    }

                    // convert to 3D matrix
                    // number of voxels
                    int xcount = (int)Math.Floor(image.XSize * image.XRes / xres);
                    int ycount = (int)Math.Floor(image.YSize * image.YRes / yres);
                    int zcount = (int)Math.Floor(image.ZSize * image.ZRes / zres);

                    Console.WriteLine("Converting 1D array to 3D...");
                    double[,,] binaryMat3D = DoseMatrixMethods.ConvertOneDToThreeD(binaryMat1D, xcount, ycount, zcount);

                    // LCSd
                    sw.Write(DistributionMetrics.CalculateLCSd(binaryMat3D).ToString() + ";");

                    // cold spot volume and sizes
                    var twoCS = CSCCL.ProcessReturnOnlySize(binaryMat3D, 66); // Cutoff value is arbitrary and not used
                    sw.Write(twoCS.Count(cs => (cs.Value * xres * yres * zres / 1000) > 0.1).ToString() + ";");
                    foreach (var cs in twoCS)
                    {
                        sw.Write((cs.Value * xres * yres * zres / 1000).ToString() + ";");
                    }
                    sw.WriteLine();
                }

                // Many cold spots
                sw.Write("Many cold spots;");
                MessageBox.Show("14 cold spots...");
                Structure Cold3 = Plan.StructureSet.Structures.First(s => s.Id == "PTV_cold2");

                for (int rep = 0; rep < reps; rep++)
                {
                    // Get accumulated matrix over 30 fx with random shifts
                    MessageBox.Show(string.Format("Repetition number: {0}", rep));

                    // first fraction
                    // get random shift vectors
                    double xshift = random.NextDouble();
                    double yshift = random.NextDouble();
                    double zshift = random.NextDouble();
                    VVector shift = new VVector(xshift, yshift, zshift);
                    VVector shiftN = shift / shift.Length; // normalized shift

                    double shiftSize = normalDist.Sample();

                    double[] cumMat = DoseMatrixMethods.GetMovedDoseOneDFromIsodose(Cold3, PTV, image, shiftSize * shiftN, xres, yres, zres); // accumulating dose matrices is faster in 1D. Convert these to 3D later.

                    // next fractions
                    for (int fx = 1; fx < Nfx; fx++)
                    {
                        // get random shift vectors
                        xshift = random.NextDouble();
                        yshift = random.NextDouble();
                        zshift = random.NextDouble();
                        shift = new VVector(xshift, yshift, zshift);
                        shiftN = shift / shift.Length; // normalized shift

                        shiftSize = normalDist.Sample();

                        double[] temp = DoseMatrixMethods.GetMovedDoseOneDFromIsodose(Cold3, PTV, image, shiftSize * shiftN, xres, yres, zres);
                        for (int i = 0; i < cumMat.Length; i++)
                        {
                            cumMat[i] += temp[i];
                        }
                    }

                    // cumMat now contains the number of fractions in which a voxel is under-dosed. For simplicity, let us assume that a voxel is underdosed over the entire treatment course only if it is underdosed in at least five fractions
                    double[] binaryMat1D = new double[cumMat.Length];
                    for (int i = 0; i < cumMat.Length; i++)
                    {
                        if (cumMat[i] >= 5)
                        {
                            binaryMat1D[i] = 1;
                        }
                        else
                        {
                            binaryMat1D[i] = 0;
                        }
                    }

                    // convert to 3D matrix
                    // number of voxels
                    int xcount = (int)Math.Floor(image.XSize * image.XRes / xres);
                    int ycount = (int)Math.Floor(image.YSize * image.YRes / yres);
                    int zcount = (int)Math.Floor(image.ZSize * image.ZRes / zres);

                    MessageBox.Show("Converting 1D array to 3D...");
                    double[,,] binaryMat3D = DoseMatrixMethods.ConvertOneDToThreeD(binaryMat1D, xcount, ycount, zcount);

                    // LCSd
                    sw.Write(DistributionMetrics.CalculateLCSd(binaryMat3D).ToString() + ";");

                    var manyCS = CSCCL.ProcessReturnOnlySize(binaryMat3D, 66); // Cutoff value is arbitrary and not used
                    sw.Write(manyCS.Count(cs => (cs.Value * xres * yres * zres / 1000) > 0.1).ToString() + ";");
                    foreach (var cs in manyCS)
                    {
                        sw.Write((cs.Value * xres * yres * zres / 1000).ToString() + ";");
                    }
                    sw.WriteLine();
                }
            }
        }


        // Prompt user to define path to save results
        private string GetSavePath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Choose csv file save location for distribution test",
                Filter = "csv files (*.csv)|*.csv"
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == true)
                return saveFileDialog.FileName;
            else
                return null;
        }
    }
}
