﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using Microsoft.Win32;

namespace MetricsTestV15
{
    class TestLocation
    {
        // private members
        private readonly PlanSetup TargetCenterPlan;
        private readonly PlanSetup TargetEdgePlan;
        private readonly PlanSetup OARClosePlan;
        private readonly PlanSetup OARFarPlan;
        private readonly Image Image;

        // Constructor
        public TestLocation(PlanSetup targetCenterPlan, PlanSetup targetEdgePlan, PlanSetup oarClosePlan, PlanSetup oarFarPlan, Image image)
        {
            this.TargetCenterPlan = targetCenterPlan;
            TargetCenterPlan.DoseValuePresentation = DoseValuePresentation.Absolute;
            this.TargetEdgePlan = targetEdgePlan;
            targetEdgePlan.DoseValuePresentation = DoseValuePresentation.Absolute;
            this.OARClosePlan = oarClosePlan;
            OARClosePlan.DoseValuePresentation = DoseValuePresentation.Absolute;
            this.OARFarPlan = oarFarPlan;
            OARFarPlan.DoseValuePresentation = DoseValuePresentation.Absolute;
            this.Image = image;
        }

        public void PrintResults()
        {
            string path = GetSavePath();

            // Define structures
            Structure PTV = this.TargetCenterPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm");
            Structure OAR = this.TargetCenterPlan.StructureSet.Structures.First(s => s.Id == "OAR");

            // write results to csv file
            using (StreamWriter sw = new StreamWriter(path))
            {
                // title line
                sw.WriteLine("Scenario: Cold spot in target center;");

                // Cold spot in target center
                sw.WriteLine("Spatial DVH");
                var sDVHTargetCenter = LocationMetrics.GetSpatialDvh(TargetCenterPlan, PTV, new List<double> { 10, 20, 30 });
                sw.Write("Dose 1;");
                foreach (var dvh in sDVHTargetCenter[0])
                {
                    sw.Write(dvh.DoseValue.Dose.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Volume 1;");
                foreach (var dvh in sDVHTargetCenter[0])
                {
                    sw.Write(dvh.Volume.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Dose 2;");
                foreach (var dvh in sDVHTargetCenter[1])
                {
                    sw.Write(dvh.DoseValue.Dose.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Volume 2;");
                foreach (var dvh in sDVHTargetCenter[1])
                {
                    sw.Write(dvh.Volume.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Dose 3;");
                foreach (var dvh in sDVHTargetCenter[2])
                {
                    sw.Write(dvh.DoseValue.Dose.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Volume 3;");
                foreach (var dvh in sDVHTargetCenter[2])
                {
                    sw.Write(dvh.Volume.ToString() + ";");
                }
                sw.WriteLine();
                sw.WriteLine();

                sw.WriteLine("DLH < 60Gy;");
                var DLHTargetCenter = LocationMetrics.GetColdOVH(TargetCenterPlan, PTV, Image, 60);
                sw.Write("Distance from edge;");
                foreach (var point in DLHTargetCenter)
                {
                    sw.Write(point.X.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Volume [cc];");
                foreach (var point in DLHTargetCenter)
                {
                    sw.Write(point.Y.ToString() + ";");
                }
                sw.WriteLine();
                sw.WriteLine();

                // Cold spot at target edge
                sw.WriteLine("Scenario: Cold spot at target edge");

                sw.WriteLine("Spatial DVH");
                var sDVHTargetEdge = LocationMetrics.GetSpatialDvh(TargetEdgePlan, PTV, new List<double> { 10, 20, 30 });
                sw.Write("Dose 1;");
                foreach (var dvh in sDVHTargetEdge[0])
                {
                    sw.Write(dvh.DoseValue.Dose.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Volume 1;");
                foreach (var dvh in sDVHTargetEdge[0])
                {
                    sw.Write(dvh.Volume.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Dose 2;");
                foreach (var dvh in sDVHTargetEdge[1])
                {
                    sw.Write(dvh.DoseValue.Dose.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Volume 2;");
                foreach (var dvh in sDVHTargetEdge[1])
                {
                    sw.Write(dvh.Volume.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Dose 3;");
                foreach (var dvh in sDVHTargetEdge[2])
                {
                    sw.Write(dvh.DoseValue.Dose.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Volume 3;");
                foreach (var dvh in sDVHTargetEdge[2])
                {
                    sw.Write(dvh.Volume.ToString() + ";");
                }
                sw.WriteLine();
                sw.WriteLine();

                sw.WriteLine("DLH < 60Gy;");
                var DLHTargetEdge = LocationMetrics.GetColdOVH(TargetEdgePlan, PTV, Image, 60);
                sw.Write("Distance from edge;");
                foreach (var point in DLHTargetEdge)
                {
                    sw.Write(point.X.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Volume [cc];");
                foreach (var point in DLHTargetEdge)
                {
                    sw.Write(point.Y.ToString() + ";");
                }
                sw.WriteLine();
                sw.WriteLine();

                // Hot spot at OAR edge
                sw.WriteLine("Scenario: Hot spot OAR close to target");

                sw.WriteLine("Spatial DVH");
                Structure PTV_ext = OARClosePlan.StructureSet.AddStructure("Control", "PTV_ext");
                PTV_ext.SegmentVolume = PTV.Margin(30);
                var sDVHOARClose = LocationMetrics.GetSpatialDvh(OARClosePlan, OAR, PTV_ext, new List<double> { 20, 50 });
                OARClosePlan.StructureSet.RemoveStructure(PTV_ext);
                sw.Write("Dose 1;");
                foreach (var dvh in sDVHOARClose[0])
                {
                    sw.Write(dvh.DoseValue.Dose.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Volume 1;");
                foreach (var dvh in sDVHOARClose[0])
                {
                    sw.Write(dvh.Volume.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Dose 2;");
                foreach (var dvh in sDVHOARClose[1])
                {
                    sw.Write(dvh.DoseValue.Dose.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Volume 2;");
                foreach (var dvh in sDVHOARClose[1])
                {
                    sw.Write(dvh.Volume.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Dose 3;");
                foreach (var dvh in sDVHOARClose[2])
                {
                    sw.Write(dvh.DoseValue.Dose.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Volume 3;");
                foreach (var dvh in sDVHOARClose[2])
                {
                    sw.Write(dvh.Volume.ToString() + ";");
                }
                sw.WriteLine();
                sw.WriteLine();

                sw.WriteLine("DLH > 60Gy;");
                var DLHOARClose = LocationMetrics.GetHotOVH(OARClosePlan, OAR, PTV, Image, 60);
                sw.Write("Distance from edge;");
                foreach (var point in DLHOARClose)
                {
                    sw.Write(point.X.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Volume [cc];");
                foreach (var point in DLHOARClose)
                {
                    sw.Write(point.Y.ToString() + ";");
                }
                sw.WriteLine();
                sw.WriteLine();

                // Hot spot at OAR far
                sw.WriteLine("Scenario: Hot spot at OAR far from target");

                sw.WriteLine("Spatial DVH");
                Structure PTV_ext1 = OARFarPlan.StructureSet.AddStructure("Control", "PTV_ext1");
                PTV_ext1.SegmentVolume = PTV.Margin(30);
                var sDVHOARFar = LocationMetrics.GetSpatialDvh(OARFarPlan, OAR, PTV_ext1, new List<double> { 20, 50 });
                OARFarPlan.StructureSet.RemoveStructure(PTV_ext1);
                sw.Write("Dose 1;");
                foreach (var dvh in sDVHOARFar[0])
                {
                    sw.Write(dvh.DoseValue.Dose.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Volume 1;");
                foreach (var dvh in sDVHOARFar[0])
                {
                    sw.Write(dvh.Volume.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Dose 2;");
                foreach (var dvh in sDVHOARFar[1])
                {
                    sw.Write(dvh.DoseValue.Dose.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Volume 2;");
                foreach (var dvh in sDVHOARFar[1])
                {
                    sw.Write(dvh.Volume.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Dose 3;");
                foreach (var dvh in sDVHOARFar[2])
                {
                    sw.Write(dvh.DoseValue.Dose.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Volume 3;");
                foreach (var dvh in sDVHOARFar[2])
                {
                    sw.Write(dvh.Volume.ToString() + ";");
                }
                sw.WriteLine();
                sw.WriteLine();

                sw.WriteLine("DLH > 60Gy;");
                var DLHOARFar = LocationMetrics.GetHotOVH(OARFarPlan, OAR, PTV, Image, 60);
                sw.Write("Distance from edge;");
                foreach (var point in DLHOARFar)
                {
                    sw.Write(point.X.ToString() + ";");
                }
                sw.WriteLine();

                sw.Write("Volume [cc];");
                foreach (var point in DLHOARFar)
                {
                    sw.Write(point.Y.ToString() + ";");
                }
                sw.WriteLine();
                sw.WriteLine();
            }
        }

        // This routine is SUPER slow. I ended up exporting the DICOM objects to MATLAB and running the robustness analysis there instead.
        public void LocationRobustness(double shiftsize, int Nfx, int reps, Image image)
        {
            string path = GetSavePath();

            // Define structures
            Structure PTV = this.TargetCenterPlan.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm");
            Structure OAR = this.TargetCenterPlan.StructureSet.Structures.First(s => s.Id == "OAR");

            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine("CS at target center");
                //System.Windows.MessageBox.Show("CS at target center");
                sw.WriteLine("Volume; Absolute minimum; 5%; 95%; Absolute maximum;");

                for (int rep = 0; rep < reps; rep++)
                {
                    //System.Windows.MessageBox.Show(string.Format("Repetition nr: {0}", rep));

                    var minMaxDist = LocationMetrics.FindMovedCSNearFarDistance(TargetCenterPlan, image, PTV, 60, 5, 30);
                    foreach (var CS in minMaxDist)
                    {
                        sw.Write(string.Format("{0}; {1}; {2}; {3}; {4}", CS.Item1,  CS.Item2[0], CS.Item2[2], CS.Item2[3], CS.Item2[1]));
                    }
                    sw.WriteLine();
                }

                sw.WriteLine("CS at target edge");
                System.Windows.MessageBox.Show("CS at target edge");
                sw.WriteLine("Volume; Absolute minimum; 5%; 95%; Absolute maximum;");

                for (int rep = 0; rep < reps; rep++)
                {
                    System.Windows.MessageBox.Show(string.Format("Repetition nr: {0}", rep));

                    var minMaxDist = LocationMetrics.FindMovedCSNearFarDistance(TargetEdgePlan, image, PTV, 60, 5, 30);
                    foreach (var CS in minMaxDist)
                    {
                        sw.Write(string.Format("{0}; {1}; {2}; {3}; {4}", CS.Item1, CS.Item2[0], CS.Item2[2], CS.Item2[3], CS.Item2[1]));
                    }
                    sw.WriteLine();
                }

                System.Windows.MessageBox.Show("HS close to target");
                sw.WriteLine("HS close to target");
                sw.WriteLine("Volume; Absolute minimum; 5%; 95%; Absolute maximum;");

                for (int rep = 0; rep < reps; rep++)
                {
                    System.Windows.MessageBox.Show(string.Format("Repetition nr: {0}", rep));

                    var minMaxDist = LocationMetrics.FindMovedCSNearFarDistance(OARClosePlan, image, PTV, 60, 5, 30);
                    foreach (var CS in minMaxDist)
                    {
                        sw.Write(string.Format("{0}; {1}; {2}; {3}; {4}", CS.Item1, CS.Item2[0], CS.Item2[2], CS.Item2[3], CS.Item2[1]));
                    }
                    sw.WriteLine();
                }

                sw.WriteLine("HS far from target");
                System.Windows.MessageBox.Show("HS far from target");
                sw.WriteLine("Volume; Absolute minimum; 5%; 95%; Absolute maximum;");

                for (int rep = 0; rep < reps; rep++)
                {
                    System.Windows.MessageBox.Show(string.Format("Repetition nr: {0}", rep));

                    var minMaxDist = LocationMetrics.FindMovedCSNearFarDistance(OARFarPlan, image, PTV, 60, 5, 30);
                    foreach (var CS in minMaxDist)
                    {
                        sw.Write(string.Format("{0}; {1}; {2}; {3}; {4}", CS.Item1, CS.Item2[0], CS.Item2[2], CS.Item2[3], CS.Item2[1]));
                    }
                    sw.WriteLine();
                }
            }
        }

        // Prompt user to define path to save results
        private string GetSavePath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Choose csv file save location for distribution test",
                Filter = "csv files (*.csv)|*.csv"
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == true)
                return saveFileDialog.FileName;
            else
                return null;
        }
    }
}
