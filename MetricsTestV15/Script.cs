using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Collections.Generic;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;

// enable writing to the database
[assembly: ESAPIScript(IsWriteable = true)]

namespace VMS.TPS
{
  public class Script
  {
        public Script()
        {
        }

        public void Execute(ScriptContext context /*, System.Windows.Window window*/)
        {
            Patient patient = context.Patient;
            patient.BeginModifications();

            // Conformity
            //var evalConf = new MetricsTestV15.TestConformity(context.PlansInScope.First(p => p.Id == "Conf_0"), context.PlansInScope.First(p => p.Id == "Conf_1"), context.PlansInScope.First(p => p.Id == "Conf_2"));
            //MessageBox.Show("Choose save location for overcoverage test:");
            //evalConf.PrintOverCovResults();
            //MessageBox.Show("Choose save location for undercoverage test:");
            //evalConf.PrintUnderCovResults();
            //MessageBox.Show("Choose save location for moved coverage test:");
            //evalConf.PrintMovedCovResults();

            ////Gradient
            //var evalGrad = new MetricsTestV15.TestGradient(context.PlansInScope.First(p => p.Id == "Grad_0"), context.PlansInScope.First(p => p.Id == "Grad_1"), context.PlansInScope.First(p => p.Id == "Grad_2"));
            //MessageBox.Show("Choose save location for isotropic expansion test:");
            //evalGrad.PrintResults100();
            //MessageBox.Show("Choose save location for 75% expansion test:");
            //evalGrad.PrintResults75();
            //MessageBox.Show("Choose save location for 50% expansion test:");
            //evalGrad.PrintResults50();

            // Directional gradient
            //var evalDirGrad = new MetricsTestV15.TestDirGrad(context.PlansInScope.First(p => p.Id == "Grad_ant"), context.PlansInScope.First(p => p.Id == "Grad_right"));
            //MessageBox.Show("Choose save location for directional expansion test:");
            //evalDirGrad.PrintResultsDirection();

            //// Homogeneity
            //var evalHom = new MetricsTestV15.TestHomogeneity(context.PlansInScope.First(p => p.Id == "Hom"));
            //MessageBox.Show("Choose save location for homogeneity test:");
            //evalHom.PrintResults();

            ////Location
            var evalLoc = new MetricsTestV15.TestLocation(context.PlansInScope.First(p => p.Id == "Location"), context.PlansInScope.First(p => p.Id == "Location1"), context.PlansInScope.First(p => p.Id == "Location2"), context.PlansInScope.First(p => p.Id == "Location3"), context.Image);
            MessageBox.Show("Choose save location for location test:");
            //evalLoc.PrintResults();
            evalLoc.LocationRobustness(5, 30, 15, context.Image);

            //// Distribution
            //var evalDist = new MetricsTestV15.TestDistribution(context.PlansInScope.First(p => p.Id == "Distribution"), context.Image);
            //MessageBox.Show("Choose save location for distribution test:");
            //evalDist.PrintResults();
            //evalDist.RobustDistribution(5, 30, 15);
        }
    }
}
