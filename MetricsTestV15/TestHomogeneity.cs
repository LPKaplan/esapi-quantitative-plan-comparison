﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using Microsoft.Win32;
using MathNet.Numerics;

namespace MetricsTestV15
{
    class TestHomogeneity
    {
        private readonly PlanSetup Plan;

        public TestHomogeneity(PlanSetup plan)
        {
            Plan = plan;
        }

        public void PrintResults()
        {
            string path = GetSavePath();

            // Define structures
            Structure sph_3 = this.Plan.StructureSet.Structures.First(s => s.Id == "PTV_sph3cm");
            Structure sph_5 = this.Plan.StructureSet.Structures.First(s => s.Id == "PTV_sph5cm");
            Structure sph_7 = this.Plan.StructureSet.Structures.First(s => s.Id == "PTV_sph7cm");
            Structure lng = this.Plan.StructureSet.Structures.First(s => s.Id == "PTV_lng");
            Structure irreg = this.Plan.StructureSet.Structures.First(s => s.Id == "PTV_irreg");

            // write results to csv file
            using (StreamWriter sw = new StreamWriter(path))
            {
                // get DVHs
                List<DVHPoint> UniDVH = new List<DVHPoint>();
                List<DVHPoint> GaussianDVH1 = new List<DVHPoint>();
                List<DVHPoint> GaussianDVH2 = new List<DVHPoint>();
                List<DVHPoint> GaussianDVH3 = new List<DVHPoint>();
                MessageBox.Show("Choose file to read first skew DVH from:");
                List<DVHPoint> SkewDVH1 = SkewProbability();
                MessageBox.Show("Choose file to read second skew DVH from:");
                List<DVHPoint> SkewDVH2 = SkewProbability();

                for (double d = 0; d <= 120; d += 0.1)
                {
                    UniDVH.Add(UniformProbabilityDVHPoint(d, sph_5.Volume));
                    GaussianDVH1.Add(GaussianProbability(d, 102, 2));
                    GaussianDVH2.Add(GaussianProbability(d, 102, 3));
                    GaussianDVH3.Add(GaussianProbability(d, 104, 2));
                }

                // Write HI results to csv file
                // title line
                sw.WriteLine("Scenario;HI_ICRU;HI_RTOG;HI_Mayo;HI_Heufelder;Dstd");

                // Uniform probability
                sw.Write("Uniform probability;");
                sw.Write(HomogeneityMetrics.HI_ICRU(UniDVH).ToString() + ";");
                sw.Write(HomogeneityMetrics.HI_RTOG(UniDVH, 100).ToString() + ";");
                sw.Write(HomogeneityMetrics.HI_Mayo(UniDVH, 100, Math.Sqrt(12)).ToString() + ";"); // std of standard uniform distribution is sqrt(1/12 (max - min)^2)
                sw.Write(HomogeneityMetrics.HI_Heufelder(101, 100, Math.Sqrt(12)).ToString() + ";");
                sw.Write(Math.Sqrt(12).ToString());
                sw.WriteLine();

                // Gaussian mean = 102, std = 2
                sw.Write("Gaussian mu=102 sig=2;");
                sw.Write(HomogeneityMetrics.HI_ICRU(GaussianDVH1).ToString() + ";");
                sw.Write(HomogeneityMetrics.HI_RTOG(GaussianDVH1, 100).ToString() + ";");
                sw.Write(HomogeneityMetrics.HI_Mayo(GaussianDVH1, 100, 2).ToString() + ";"); // std of standard uniform distribution is sqrt(1/12 (max - min)^2)
                sw.Write(HomogeneityMetrics.HI_Heufelder(102, 100, 2).ToString() + ";");
                sw.Write(2.ToString());
                sw.WriteLine();

                // Gaussian mean = 102, std = 3
                sw.Write("Gaussian mu=102 sig=3;");
                sw.Write(HomogeneityMetrics.HI_ICRU(GaussianDVH2).ToString() + ";");
                sw.Write(HomogeneityMetrics.HI_RTOG(GaussianDVH2, 100).ToString() + ";");
                sw.Write(HomogeneityMetrics.HI_Mayo(GaussianDVH2, 100, 3).ToString() + ";"); // std of standard uniform distribution is sqrt(1/12 (max - min)^2)
                sw.Write(HomogeneityMetrics.HI_Heufelder(102, 100, 3).ToString() + ";");
                sw.Write(3.ToString());
                sw.WriteLine();

                // Gaussian mean = 104, std = 2
                sw.Write("Gaussian mu=104 sig=2;");
                sw.Write(HomogeneityMetrics.HI_ICRU(GaussianDVH3).ToString() + ";");
                sw.Write(HomogeneityMetrics.HI_RTOG(GaussianDVH3, 100).ToString() + ";");
                sw.Write(HomogeneityMetrics.HI_Mayo(GaussianDVH3, 100, 2).ToString() + ";"); // std of standard uniform distribution is sqrt(1/12 (max - min)^2)
                sw.Write(HomogeneityMetrics.HI_Heufelder(103, 100, 2).ToString() + ";");
                sw.Write(2.ToString());
                sw.WriteLine();

                // Skew normal xi = 102, omega = 2, alpha = 4
                sw.Write("Skew normal xi = 102 omega = 2 alpha = 4;");
                sw.Write(HomogeneityMetrics.HI_ICRU(SkewDVH1).ToString() + ";");
                sw.Write(HomogeneityMetrics.HI_RTOG(SkewDVH1, 100).ToString() + ";");
                sw.Write(HomogeneityMetrics.HI_Mayo(SkewDVH1, 100, CalculateSkewStd(102, 2, 4)).ToString() + ";"); // std of standard uniform distribution is sqrt(1/12 (max - min)^2)
                sw.Write(HomogeneityMetrics.HI_Heufelder(CalculateSkewMean(102, 2, 4), 100, CalculateSkewStd(102, 2, 4)).ToString() + ";");
                sw.Write(CalculateSkewStd(102, 2, 4).ToString());
                sw.WriteLine();

                // Skew normal xi = 102, omega = 2, alpha = -4
                sw.Write("Skew normal xi = 102 omega = 2 alpha = 4;");
                sw.Write(HomogeneityMetrics.HI_ICRU(SkewDVH2).ToString() + ";");
                sw.Write(HomogeneityMetrics.HI_RTOG(SkewDVH2, 100).ToString() + ";");
                sw.Write(HomogeneityMetrics.HI_Mayo(SkewDVH2, 100, CalculateSkewStd(102, 2, -4)).ToString() + ";"); // std of standard uniform distribution is sqrt(1/12 (max - min)^2)
                sw.Write(HomogeneityMetrics.HI_Heufelder(CalculateSkewMean(102, 2, -4), 100, CalculateSkewStd(102, 2, -4)).ToString() + ";");
                sw.Write(CalculateSkewStd(102, 2, -4).ToString());
                sw.WriteLine();
            }
        }

        // Prompt user to define path to save results
        private string GetSavePath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Choose csv file save location for homogeneity test",
                Filter = "csv files (*.csv)|*.csv"
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == true)
                return saveFileDialog.FileName;
            else
                return null;
        }

        // Generate DVH methods
        private DVHPoint UniformProbabilityDVHPoint(double relDose, double targetVolume)
        {
            if (relDose <= 94.8773)
            {
                return new DVHPoint(new DoseValue(relDose, DoseValue.DoseUnit.Percent), 100, "Percent");
            }
            else if (relDose > 94.8773 & relDose <= 107)
            {
                double a = ((1.8 / targetVolume * 100) - 99.0) / (107.0 - 95.0);    // line inclination
                double b = 99.0 - a * 95.0; // line intercept at x = 0
                return new DVHPoint(new DoseValue(relDose, DoseValue.DoseUnit.Percent), ((a * relDose) + b), "Percent");
            }
            else
            {
                return new DVHPoint(new DoseValue(relDose, DoseValue.DoseUnit.Percent), 0, "Percent");
            }
        }

        private DVHPoint GaussianProbability(double relDose, double mean, double sd)
        {
            return new DVHPoint(new DoseValue(relDose, DoseValue.DoseUnit.Percent), 100 - 100 * 1 / 2 * (1 + SpecialFunctions.Erf((relDose - mean) / (sd * Math.Sqrt(2)))), "Percent");
        }

        private List<DVHPoint> SkewProbability()
        {
            string path = GetFilePath();
            List<DVHPoint> returnList = new List<DVHPoint>();
            double dose;
            double volume;

            // For now read DVH created in MATLAB. Maybe implement calculation later
            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.Peek() >= 0)
                {
                    string line = sr.ReadLine();
                    string[] splitLine = line.Split('\t');
                    double.TryParse(splitLine[0], out dose);
                    double.TryParse(splitLine[1], out volume);
                    returnList.Add(new DVHPoint(new DoseValue(dose, DoseValue.DoseUnit.Percent), volume, "Percent"));
                }
            }

            return returnList;
        }

        private double CalculateSkewMean(double xi, double omega, double alpha)
        {
            return xi + omega * (alpha / Math.Sqrt(1 + Math.Pow(alpha, 2)));
        }

        private double CalculateSkewStd(double xi, double omega, double alpha)
        {
            return Math.Sqrt(Math.Pow(omega, 2) * (1 - 2 * Math.Pow((alpha / Math.Sqrt(1 + Math.Pow(alpha, 2))), 2) / Math.PI));
        }

        // prompt user to specify skew DVH file
        private string GetFilePath()
        {
            var openFileDialog = new OpenFileDialog
            {
                Title = "Choose data file",
                Filter = "txt files (*.txt)|*.txt"
            };

            var dialogResult = openFileDialog.ShowDialog();

            if (dialogResult == true)
                return openFileDialog.FileName;
            else
                return null;
        }
    }
}
