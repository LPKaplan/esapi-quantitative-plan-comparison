﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using MoreLinq;

namespace MetricsTestV15
{
    public static class HomogeneityMetrics
    {
        public static double HI_ICRU(List<DVHPoint> dvh)
        {
            double D2 = dvh.MinBy(p => Math.Abs(p.Volume - 2)).First().DoseValue.Dose;
            double D98 = dvh.MinBy(p => Math.Abs(p.Volume - 98)).First().DoseValue.Dose;
            double D50 = dvh.MinBy(p => Math.Abs(p.Volume - 50)).First().DoseValue.Dose;
            return (D2 - D98) / D50;
        }

        public static double HI_RTOG(List<DVHPoint> dvh, double Dp)
        {
            List<DVHPoint> dvhPos = dvh.Where(p => p.Volume > 0).ToList();
            double Dmax = dvhPos.MaxBy(p => p.DoseValue.Dose).First().DoseValue.Dose;
            return Dmax / Dp;
        }

        public static double HI_Mayo(List<DVHPoint> dvh, double Dp, double Dstd)
        {
            List<DVHPoint> dvhPos = dvh.Where(p => p.Volume > 0).ToList();
            double Dmax = dvhPos.MaxBy(p => p.DoseValue.Dose).First().DoseValue.Dose;
            return (Dmax / Dp) * (Dstd / Dp);
        }

        public static double HI_Heufelder(double Dmean, double Dp, double Dstd)
        {
            return Math.Exp(-1 * Math.Pow(0.1 * (1 - Dmean / Dp), 2)) * Math.Exp(-1 * 0.1 * Dstd);
        }
    }
}
