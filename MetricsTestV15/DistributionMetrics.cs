﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using OxyPlot;
using ConnectedComponentLabeling;

namespace MetricsTestV15
{
    public static class DistributionMetrics
    {
        public static double CalculateLCSd(PlanSetup plan, Structure structure, Structure isodose, Image image)
        // This function calculates a penalty for large cold spots in the target volume. It is the same as the LCS metric proposed by Said et al
        {
            plan.DoseValuePresentation = DoseValuePresentation.Absolute;

            var dose = plan.Dose;
            var binaryDoseMatrix = DoseMatrixMethods.GetDoseThreeDFromIsodose(isodose, structure, image, 1, 1, 1);

            // Get dictionary with label and cold spot blob matrices
            var ccl = new BinaryCCL();
            var ColdSpots = ccl.ProcessReturnOnlySize(binaryDoseMatrix, 66); // Arbitrary cutoff value is not used here

            int count = 0;
            foreach (var vox in binaryDoseMatrix)
            {
                if (vox > 0)
                {
                    count += 1;
                }
            }

            double Nom = 0;

            foreach (KeyValuePair<int, int> cs in ColdSpots) // each dictionary entry is the blob's label as key and its size (in voxels) as value
            { 
                Nom += Math.Pow(cs.Value, 2);
            }

            double LCSd = Nom / (Math.Pow(count, 2));

            return LCSd;
        }

        public static double CalculateLCSd(double[,,] binaryDoseMatrix)
        // This function calculates a penalty for large cold spots in the target volume. It is the same as the LCS metric proposed by Said et al
        {
            // Get dictionary with label and cold spot blob matrices
            var ccl = new BinaryCCL();
            var ColdSpots = ccl.ProcessReturnOnlySize(binaryDoseMatrix, 66); // Arbitrary cutoff value is not used here

            int count = 0;
            foreach (var vox in binaryDoseMatrix)
            {
                if (vox > 0)
                {
                    count += 1;
                }
            }

            double Nom = 0;

            foreach (KeyValuePair<int, int> cs in ColdSpots) // each dictionary entry is the blob's label as key and its size (in voxels) as value
            {
                Nom += Math.Pow(cs.Value, 2);
            }

            double LCSd = Nom / (Math.Pow(count, 2));

            return LCSd;
        }

        // Helper methods


        // Helper methods



    }
}
