﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Media3D;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;
using ConnectedComponentLabeling;
using OxyPlot;

namespace Metrics
{
    // These metrics quantify dose texture properties
    public static class TextureMetrics
    {
        /// <summary>
        /// Method to find contiguous cold spots in a given structure. 
        /// </summary>
        /// <param name="plan">PlanSetup</param>
        /// <param name="structure">Structure</param>
        /// <param name="cutoff">double</param>
        /// <returns>double[] listing sizes (in cc) of each individual spot.</returns>
        public static double[] FindColdSpots(PlanSetup plan, Structure structure, double cutoff)
        {
            var dose = plan.Dose;

            // resolution of queried dose matrix in mm
            double xres = 1;
            double yres = 1;
            double zres = 1;

            var binaryDoseMatrix = DoseMatrixExtract.CreateBinaryDoseMatrix(dose, structure, cutoff, xres, yres, zres, true);

            // Get dictionary with label and cold spot blob matrices
            var ccl = new BinaryCCL();
            var ColdSpots = ccl.Process(binaryDoseMatrix, cutoff);

            var result = new List<double>();


            foreach (var item in ColdSpots)
            {
                double sum = 0;
                foreach (var val in item.Value)
                {
                    if (val > 0) // if the voxel is not background
                        sum += (1 * xres * yres * zres / 1000); // the volume of a voxel in cc
                }

                result.Add(sum);
            }

            // the items are the sizes (in voxels) of the spots.
            return result.ToArray();
        }

        /// <summary>
        /// Overload to find cold spots in PlanUncertainties.
        /// </summary>
        /// <param name="upln"></param>
        /// <param name="structure"></param>
        /// <param name="cutoff"></param>
        /// <returns></returns>
        public static double[] FindColdSpots(PlanUncertainty upln, Structure structure, double cutoff)
        {
            var dose = upln.Dose;
            // resolution of queried dose matrix in mm
            double xres = 1;
            double yres = 1;
            double zres = 1;

            var binaryDoseMatrix = DoseMatrixExtract.CreateBinaryDoseMatrix(dose, structure, cutoff, xres, yres, zres, true);

            // Get dictionary with label and cold spot blob matrices
            var ccl = new BinaryCCL();
            var ColdSpots = ccl.Process(binaryDoseMatrix, cutoff);

            var result = new List<double>();


            foreach (var item in ColdSpots)
            {
                double sum = 0;
                foreach (var val in item.Value)
                {
                    if (val > 0) // if the voxel is not background
                        sum += (1 * xres * yres * zres) / 1000; // the volume of a voxel in cc
                }

                result.Add(sum);
            }

            // the items are the sizes (in voxels) of the spots.
            return result.ToArray();
        }

        /// <summary>
        /// Method to find contiguous hot spots in a given structure. 
        /// </summary>
        /// <param name="plan">PlanSetup</param>
        /// <param name="structure">Structure</param>
        /// <param name="cutoff">double</param>
        /// <returns>double[] listing sizes (in cc) of each individual spot.</returns>
        public static double[] FindHotSpots(PlanSetup plan, Structure structure, double cutoff)
        {
            var dose = plan.Dose;
            // resolution of queried dose matrix in mm
            double xres = 1;
            double yres = 1;
            double zres = 1;

            var binaryDoseMatrix = DoseMatrixExtract.CreateBinaryDoseMatrix(dose, structure, cutoff, xres, yres, zres, false);

            // Get dictionary with label and cold spot blob matrices
            var ccl = new BinaryCCL();
            var ColdSpots = ccl.Process(binaryDoseMatrix, cutoff);

            var result = new List<double>();


            foreach (var item in ColdSpots)
            {
                double sum = 0;
                foreach (var val in item.Value)
                {
                    if (val > 0) // if the voxel is not background
                        sum += (1 * xres * yres * zres) / 1000; // the volume of a voxel in cc
                }

                result.Add(sum);
            }

            // the items are the sizes (in voxels) of the spots.
            return result.ToArray();
        }

        /// <summary>
        /// Overload to find cold spots in PlanUncertainties.
        /// </summary>
        /// <param name="upln"></param>
        /// <param name="structure"></param>
        /// <param name="cutoff"></param>
        /// <returns></returns>
        public static double[] FindHotSpots(PlanUncertainty upln, Structure structure, double cutoff)
        {
            var dose = upln.Dose;

            // resolution of queried dose matrix in mm
            double xres = 1;
            double yres = 1;
            double zres = 1;

            var binaryDoseMatrix = DoseMatrixExtract.CreateBinaryDoseMatrix(dose, structure, cutoff, xres, yres, zres, false);

            // Get dictionary with label and cold spot blob matrices
            var ccl = new BinaryCCL();
            var ColdSpots = ccl.Process(binaryDoseMatrix, cutoff);

            var result = new List<double>();


            foreach (var item in ColdSpots)
            {
                double sum = 0;
                foreach (var val in item.Value)
                {
                    if (val > 0) // if the voxel is not background
                        sum += (1 * xres * yres * zres) / 1000; // the volume of a voxel in cc
                }

                result.Add(sum);
            }

            // the items are the sizes (in voxels) of the spots.
            return result.ToArray();
        }

        /// <summary>
        /// This method calculates a penalty for large cold spots in the target volume. It is the same as the LCSd metric proposed by Said et al
        /// </summary>
        /// <param name="plan"></param>
        /// <param name="structure"></param>
        /// <param name="cutoff"></param>
        /// <returns></returns>
        public static double CalculateLCSd(PlanSetup plan, Structure structure, double cutoff)
        {
            plan.DoseValuePresentation = DoseValuePresentation.Absolute;

            var dose = plan.Dose;

            // resolution of queried dose matrix in mm
            double xres = 1;
            double yres = 1;
            double zres = 1;

            var binaryDoseMatrix = DoseMatrixExtract.CreateBinaryDoseMatrix(dose, structure, cutoff, xres, yres, zres, true);

            // Get dictionary with label and cold spot blob matrices
            var ccl = new BinaryCCL();
            var ColdSpots = ccl.ProcessReturnOnlySize(binaryDoseMatrix, cutoff);

            int count = 0;
            foreach (var vox in binaryDoseMatrix)
            {
                if (vox > 0)
                {
                    count += 1;
                }
            }

            //string message = "";

            //foreach (var cs in ColdSpots)
            //{
            //    message += string.Format("{0}\n", cs.Value);
            //}

            //message += string.Format("\n\nSum: {0}\n\n", ColdSpots.Values.Sum());

            //message += string.Format("Voxelcount: {0}", count);
            //MessageBox.Show(message);

            double Nom = 0;

            foreach (KeyValuePair<int, int> cs in ColdSpots) // each dictionary entry is a double dose value as Key and a list of blob sizes (in voxels) as Value
            {
                Nom += Math.Pow(cs.Value, 2);
            }

            double LCSd = Nom / (Math.Pow(count, 2));

            return LCSd;
        }

        /// <summary>
        /// Overload for PlanUncertainties. This Method calculates a penalty for large cold spots in the target volume. It is the same as the LCSd metric proposed by Said et al
        /// </summary>
        /// <param name="uplan"></param>
        /// <param name="structure"></param>
        /// <param name="cutoff"></param>
        /// <returns></returns>
        public static double CalculateLCSd(PlanUncertainty uplan, Structure structure, double cutoff)
        {
            var dose = uplan.Dose;

            // resolution of queried dose matrix in mm
            double xres = 1;
            double yres = 1;
            double zres = 1;

            var binaryDoseMatrix = DoseMatrixExtract.CreateBinaryDoseMatrix(dose, structure, cutoff, xres, yres, zres, true);

            // Get dictionary with label and cold spot blob matrices
            var ccl = new BinaryCCL();
            var ColdSpots = ccl.ProcessReturnOnlySize(binaryDoseMatrix, cutoff);

            int count = 0;
            foreach (var vox in binaryDoseMatrix)
            {
                if (vox > 0)
                {
                    count += 1;
                }
            }

            //string message = "";

            //foreach (var cs in ColdSpots)
            //{
            //    message += string.Format("{0}\n", cs.Value);
            //}

            //message += string.Format("\n\nSum: {0}\n\n", ColdSpots.Values.Sum());

            //message += string.Format("Voxelcount: {0}", count);
            //MessageBox.Show(message);

            double Nom = 0;

            foreach (KeyValuePair<int, int> cs in ColdSpots) // each dictionary entry is a double dose value as Key and a list of blob sizes (in voxels) as Value
            {
                Nom += Math.Pow(cs.Value, 2);
            }

            double LCSd = Nom / (Math.Pow(count, 2));

            return LCSd;
        }

        /// <summary>
        /// This Method calculates a penalty for large hot spots in a volume. It is the derived from the LCSd metric proposed by Said et al
        /// </summary>
        /// <param name="uplan"></param>
        /// <param name="structure"></param>
        /// <param name="cutoff"></param>
        /// <returns></returns>
        public static double CalculateLHSd(PlanSetup plan, Structure structure, double cutoff)
        {
            plan.DoseValuePresentation = DoseValuePresentation.Absolute;

            var dose = plan.Dose;

            // resolution of queried dose matrix in mm
            double xres = 1;
            double yres = 1;
            double zres = 1;

            var binaryDoseMatrix = DoseMatrixExtract.CreateBinaryDoseMatrix(dose, structure, cutoff, xres, yres, zres, false);

            // Get dictionary with label and cold spot blob matrices
            var ccl = new BinaryCCL();
            var ColdSpots = ccl.ProcessReturnOnlySize(binaryDoseMatrix, cutoff);

            int count = 0;
            foreach (var vox in binaryDoseMatrix)
            {
                if (vox > 0)
                {
                    count += 1;
                }
            }

            //string message = "";

            //foreach (var cs in ColdSpots)
            //{
            //    message += string.Format("{0}\n", cs.Value);
            //}

            //message += string.Format("\n\nSum: {0}\n\n", ColdSpots.Values.Sum());

            //message += string.Format("Voxelcount: {0}", count);
            //MessageBox.Show(message);

            double Nom = 0;

            foreach (KeyValuePair<int, int> cs in ColdSpots) // each dictionary entry is a double dose value as Key and a list of blob sizes (in voxels) as Value
            {
                Nom += Math.Pow(cs.Value, 2);
            }

            double LCSd = Nom / (Math.Pow(count, 2));

            return LCSd;
        }

        /// <summary>
        /// Overload for PlanUncertainties. This Method calculates a penalty for large hot spots in a volume. It is the derived from the LCSd metric proposed by Said et al
        /// </summary>
        /// <param name="uplan"></param>
        /// <param name="structure"></param>
        /// <param name="cutoff"></param>
        /// <returns></returns>
        public static double CalculateLHSd(PlanUncertainty uplan, Structure structure, double cutoff)
        {
            var dose = uplan.Dose;

            // resolution of queried dose matrix in mm
            double xres = 1;
            double yres = 1;
            double zres = 1;

            var binaryDoseMatrix = DoseMatrixExtract.CreateBinaryDoseMatrix(dose, structure, cutoff, xres, yres, zres, false);

            // Get dictionary with label and cold spot blob matrices
            var ccl = new BinaryCCL();
            var ColdSpots = ccl.ProcessReturnOnlySize(binaryDoseMatrix, cutoff);

            int count = 0;
            foreach (var vox in binaryDoseMatrix)
            {
                if (vox > 0)
                {
                    count += 1;
                }
            }

            string message = "";

            foreach (var cs in ColdSpots)
            {
                message += string.Format("{0}\n", cs.Value);
            }

            message += string.Format("\n\nSum: {0}\n\n", ColdSpots.Values.Sum());

            message += string.Format("Voxelcount: {0}", count);
            MessageBox.Show(message);

            double Nom = 0;

            foreach (KeyValuePair<int, int> cs in ColdSpots) // each dictionary entry is a double dose value as Key and a list of blob sizes (in voxels) as Value
            {
                Nom += Math.Pow(cs.Value, 2);
            }

            double LCSd = Nom / (Math.Pow(count, 2));

            return LCSd;
        }
    }
}
