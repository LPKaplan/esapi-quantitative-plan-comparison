These are scripts for the Eclipse Scripting API written during the course of my PhD project. Their aim is to facilitate quantitative evaluation and comparison of radiotherapy treatment plans.

Contents:

- PlanComparison V13/V15: The actual binary plugin scripts to be run in Eclipse v. 13.7 or 15.6. The compiled binary plugins are in bin/debug.

- MetricsV13/V15: Class libraries containing methods that are used to calculate the metrics that can be visualized using the PlanComparison scripts.

- Connected-Component-Labeling-Algorithm-master: Class library containing the connected component labelling algorithm used in some of the calculation methods in the Metrics libraries. 

- MetricsTest: Script used to test metrics in extreme dose distribution variations as described in "A systematically compiled set of quantitative metrics to describe spatial characteristics of radiotherapy dose distributions and aid in treatment planning" (submitted to Physica Medica)

- PhantomDICOM: CT, RTStruct, RTPlan, and RTDose DICOM files of the phantom described in "A systematically compiled set of quantitative metrics to describe spatial characteristics of radiotherapy dose distributions and aid in treatment planning"