﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.IO;
using ConnectedComponentLabeling;

namespace UnitTests
{
    [TestClass()]
    public class ConnectedComponentLabelingTest
    {
        #region Member Variables

        private readonly string _baseDirectory;//, _inputDirectory;
        private string _outputDirectory;
        private readonly BinaryCCL _binaryConnectedComponentLabelling;
        private readonly CCL _CCL;

        #endregion

        #region Constructor

        public ConnectedComponentLabelingTest()
        {
            _baseDirectory = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.FullName;
            //_inputDirectory = Path.Combine(_baseDirectory, "Input");
            _binaryConnectedComponentLabelling = new BinaryCCL();
            _CCL = new CCL();
        }

        #endregion

        #region Test Initialize

        [TestInitialize()]
        public void Init()
        {
            _outputDirectory = Path.Combine(_baseDirectory, "Output");
        }

        #endregion

        #region Test Methods

        [TestMethod()]
        public void Process_OneEntityTest()
        {
            //Arrange
            double[,,] input = new double[10, 10, 10];

            for (int i = 0; i < 10; i += 1)
                for (int j = 0; j < 10; j++)
                    for (int k = 0; k < 10; k++)
                        if (4 <= i & i <= 6 & 4 <= j & j <= 6 & 4 <= k & k <= 6)
                            input[i, j, k] = 60;
                        else
                            input[i, j, k] = 30;

            _outputDirectory = Path.Combine(Path.Combine(_baseDirectory, "output"));
            Directory.CreateDirectory(_outputDirectory);

            //Act
            var images = _binaryConnectedComponentLabelling.Process(input,50);

            var maxSize = images.Keys.Count;

            foreach (var image in images)
            {
                System.IO.File.AppendAllLines(_outputDirectory + "one.txt", new List<string> { image.Key.ToString(), image.Value.Length.ToString() });
            }

            //Assert
            Assert.AreEqual(1, images.Count);
        }

        [TestMethod()]
        public void GLSZMTest()
        {
            // Arrange
            double[,,] input = new double[10, 10, 10];

            for (int i = 0; i < 10; i += 1)
                for (int j = 0; j < 10; j++)
                    for (int k = 0; k < 10; k++)
                    {
                        if (4 <= i & i <= 6 & 4 <= j & j <= 6 & 4 <= k & k <= 6)
                            input[i, j, k] = 2;
                        else
                            input[i, j, k] = 0;

                        if (i == 5 & j == 5 & k == 5)
                            input[i, j, k] = 3;
                    }

            // Act
            Dictionary<Tuple<int, int>, int> GLSZM = new Dictionary<Tuple<int, int>, int>();

            for (int i = 0; i < 5; i++)
            {
                var blobSizes = _CCL.ProcessReturnOnlySize(input, i);
                if (blobSizes.Count != 0)
                {
                    // create glszm
                    for (int z = blobSizes.Values.Min(); z <= blobSizes.Values.Max(); z++)
                    {
                        int count = blobSizes.Count(kvp => kvp.Value == z);

                        GLSZM.Add(new Tuple<int, int>(i, z), count); // return a list of all separate blob sizes for each dose level
                    }
                }
            }

            // Assert
            Assert.AreEqual(GLSZM.ElementAt(2), new KeyValuePair<Tuple<int, int>, int>(new Tuple<int, int>(3, 1), 1));
        }

        [TestMethod()]
        public void LCSTest()
        {
            // Arrange
            double[,,] input = new double[10, 10, 10];

            for (int i = 0; i < 10; i += 1)
                for (int j = 0; j < 10; j++)
                    for (int k = 0; k < 10; k++)
                    {
                        if (4 <= i & i <= 6 & 4 <= j & j <= 6 & 4 <= k & k <= 6)
                            input[i, j, k] = 2;
                        else
                            input[i, j, k] = 0;

                        if (i == 5 & j == 5 & k == 5)
                            input[i, j, k] = 3;
                    }

            double expected = (1 * Math.Pow(3 - 3, 2) * Math.Pow(1, 2) + 1 * Math.Pow(3 - 2, 2) * Math.Pow(26, 2) + 1 * Math.Pow(3 - 0, 2) * Math.Pow(1000 - 27, 2)) / (3*Math.Pow(1000,2));
            // Act

            // get GLSZM
            Dictionary<Tuple<int, int>, int> GLSZM = new Dictionary<Tuple<int, int>, int>();

            for (int i = 0; i < 5; i++)
            {
                var blobSizes = _CCL.ProcessReturnOnlySize(input, i);
                if (blobSizes.Count != 0)
                {
                    for (int z = blobSizes.Values.Min(); z <= blobSizes.Values.Max(); z++)
                    {
                        int count = blobSizes.Count(kvp => kvp.Value == z);

                        GLSZM.Add(new Tuple<int, int>(i, z), count); // return a list of all separate blob sizes for each dose level
                    }
                }
            }

            // calculate LCS
            double Nom = 0;
            int blobCount = 0;
            int maxBin = 3;
            int numberOfVoxels = input.Length;

            foreach (KeyValuePair<Tuple<int, int>, int> entry in GLSZM)
            {
                Nom += (entry.Value * Math.Pow(maxBin - entry.Key.Item1, 2) * Math.Pow(entry.Key.Item2, 2));
                blobCount += entry.Value;
            }

            double LCS = Nom / (Math.Pow(numberOfVoxels, 2) * blobCount);

            // Assert
            Assert.AreEqual(expected, LCS);
        }

        [TestMethod()]
        public void LCSdTest()
        {
            // Arrange
            double[,,] input = new double[10, 10, 10];

            for (int i = 0; i < 10; i += 1)
                for (int j = 0; j < 10; j++)
                    for (int k = 0; k < 10; k++)
                    {
                        if (1 <= i & i <= 3 & 1 <= j & j <= 3 & 1 <= k & k <= 3)
                            input[i, j, k] = 1;
                        else
                            input[i, j, k] = 0;

                        if (i == 5 & j == 5 & k == 5)
                            input[i, j, k] = 1;

                        if (i == 7 & j == 7 & k == 7)
                            input[i, j, k] = 1;
                    }

            double expected = (2 * Math.Pow(1, 2)  + 1 * Math.Pow(27, 2)) / Math.Pow(29, 2);
            // Act

           
            var ColdSpots = _binaryConnectedComponentLabelling.ProcessReturnOnlySize(input, 1);
            int count = 0;
            foreach (var vox in input)
            {
                if (vox > 0)
                {
                    count += 1;
                }
            }


            double Nom = 0;

            foreach (KeyValuePair<int, int> cs in ColdSpots) // each dictionary entry is a double dose value as Key and a list of blob sizes (in voxels) as Value
            {
                Nom += Math.Pow(cs.Value, 2);
            }

            double LCSd = Nom / (Math.Pow(count, 2));

            Console.WriteLine(string.Format("Exp: {0}, Actual: {1}", expected, LCSd));

            // Assert
            Assert.AreEqual(expected, LCSd);
        }

        //[TestMethod()]
        //public void Process_TwoEntitiesTest()
        //{
        //    //Arrange
        //    Bitmap input = new Bitmap(Path.Combine(_inputDirectory, "Two.bmp"));
        //    _outputDirectory = Path.Combine(_outputDirectory, "Two");
        //    Directory.CreateDirectory(_outputDirectory);

        //    //Act
        //    var images = _connectedComponentLabelling.Process(input);

        //    foreach (var image in images)
        //    {
        //        image.Value.Save(Path.Combine(_outputDirectory, image.Key + ".bmp"));
        //    }

        //    //Assert
        //    Assert.AreEqual(2, images.Count);
        //}

        //[TestMethod()]
        //public void Process_SixEntitiesTest()
        //{
        //    //Arrange
        //    Bitmap input = new Bitmap(Path.Combine(_inputDirectory, "Six.bmp"));
        //    _outputDirectory = Path.Combine(_outputDirectory, "Six");
        //    Directory.CreateDirectory(_outputDirectory);

        //    //Act
        //    var images = _connectedComponentLabelling.Process(input);

        //    foreach (var image in images)
        //    {
        //        image.Value.Save(Path.Combine(_outputDirectory, image.Key + ".bmp"));
        //    }

        //    //Assert
        //    Assert.AreEqual(6, images.Count);
        //}

        #endregion
    }
}