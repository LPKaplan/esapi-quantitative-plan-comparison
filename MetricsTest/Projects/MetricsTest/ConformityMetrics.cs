﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Media3D;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;

namespace MetricsTest
{
    public static class ConformityMetrics
    {
        // PITV as proposed in RTOG guidelines
        public static double CI_PITV(Structure target, Structure refIsodose)
        {
            return refIsodose.Volume / target.Volume;
        }

        // Lomax & Scheib 2003
        public static double CI_LS(PlanSetup plan, Structure target, Structure refIsodose)
        {
            if (plan.StructureSet.CanAddStructure("CONTROL", "Overlap"))
            {
                // generate structure Isodose U target
                Structure overlap = plan.StructureSet.AddStructure("CONTROL", "Overlap");
                overlap.SegmentVolume = target.And(refIsodose);

                double Vtref = overlap.Volume;
                plan.StructureSet.RemoveStructure(overlap); // remember to remove structure again

                return Vtref / refIsodose.Volume;
            }
            else
            {
                MessageBox.Show("Could not generate overlap structure!");
                return 0;
            }
        }

        // Conformity Number ICRU, Paddick 2000, van't Riet et al 1997
        public static double CI_CN(PlanSetup plan, Structure target, Structure refIsodose)
        {
            if (plan.StructureSet.CanAddStructure("CONTROL", "Overlap"))
            {
                // generate structure Isodose U target
                Structure overlap = plan.StructureSet.AddStructure("CONTROL", "Overlap");
                overlap.SegmentVolume = target.And(refIsodose);

                double Vtref = overlap.Volume;
                plan.StructureSet.RemoveStructure(overlap); // remember to remove structure again

                return Vtref / target.Volume * Vtref/refIsodose.Volume;
            }
            else
            {
                MessageBox.Show("Could not generate overlap structure!");
                return 0;
            }
        }

        // Lefkopoulos et al 2000
        public static double CI_geomC(PlanSetup plan, Structure target, Structure refIsodose)
        {
            if (plan.StructureSet.CanAddStructure("CONTROL", "OverlapCold"))
            {
                // generate structure Isodose SUB target and RVR U Isodose
                Structure targetCold = plan.StructureSet.AddStructure("CONTROL", "OverlapCold");
                targetCold.SegmentVolume = target.Sub(refIsodose);
                Structure rvrHot = plan.StructureSet.AddStructure("CONTROL", "OverlapHot");
                rvrHot.SegmentVolume = refIsodose.Sub(target);

                // get volumes
                double Vtcold = targetCold.Volume;
                double Vhot = rvrHot.Volume;

                plan.StructureSet.RemoveStructure(targetCold); // remember to remove structure again
                plan.StructureSet.RemoveStructure(rvrHot); // remember to remove structure again

                return Vtcold / target.Volume + Vhot / target.Volume;
            }
            else
            {
                MessageBox.Show("Could not generate overlap structure!");
                return 0;
            }
        }


        // DICE similarity coefficient
        public static double CI_DICE(PlanSetup plan, Structure target, Structure refIsodose)
        {
            if (plan.StructureSet.CanAddStructure("CONTROL", "Overlap"))
            {
                // generate structure Isodose U target
                Structure overlap = plan.StructureSet.AddStructure("CONTROL", "Overlap");
                overlap.SegmentVolume = target.And(refIsodose);

                double Vtref = overlap.Volume;
                plan.StructureSet.RemoveStructure(overlap); // remember to remove structure again

                return 2 * Vtref / (target.Volume + refIsodose.Volume);
            }
            else
            {
                MessageBox.Show("Could not generate overlap structure!");
                return 0;
            }
        }

        // Park et al 2014
        public static double CI_dist(Structure target, Structure refIsodose)
        {
            // Define structure COM
            VVector COM = target.CenterPoint;

            // Define radius for line end points
            double R = 100; // in mm

            // Define end points for search lines
            List<VVector> endPoints = CreateSpherePoints(COM, R);

            // Get structure and dose segments along lines.
            List<Tuple<VVector, VVector>> distances = new List<Tuple<VVector, VVector>>();

            // pre-allocate memory
            System.Collections.BitArray segmentStride = new System.Collections.BitArray(1000); // 0.1mm resolution?
            System.Collections.BitArray segmentStride1 = new System.Collections.BitArray(1000); // 0.1mm resolution?

            bool isCovered = true; // if target is not fully covered this is set to false and the return value will be negative

            for (int j = 0; j < endPoints.Count(); j++)
            {
                var endPoint = endPoints.ElementAt(j);
                SegmentProfile segmentProfile = target.GetSegmentProfile(COM, endPoint, segmentStride);
                SegmentProfile segmentProfile1 = refIsodose.GetSegmentProfile(COM, endPoint, segmentStride1);
                VVector start = COM;
                VVector end = COM;

                // If COM is inside the structure
                if (target.IsPointInsideSegment(COM))
                {
                    // Do the line searches from point where line exits structure until queried dose is reached. Save to list.
                    start = segmentProfile.Last(pp => pp.Value).Position; // furthest target point from COM

                    // if coverage is really bad and COM is outside isodose it doesn't make much sense to calculate a conformity index. Just throw a really low value.
                    if (!refIsodose.IsPointInsideSegment(COM))
                    {
                        if (!segmentProfile1.Any(pp => pp.Value))
                        {
                            VVector endPoint2 = COM - (endPoint - COM); // Look in the opposite direction
                            System.Collections.BitArray segmentStride2 = new System.Collections.BitArray(1000); // 0.1mm resolution?
                            SegmentProfile segmentProfile2 = refIsodose.GetSegmentProfile(COM, endPoint2, segmentStride2);

                            if (!segmentProfile2.Any(pp => pp.Value)) // if isodoseline does not lie in the opposite direction either. This will almost never happen in practice
                            {
                                end = segmentProfile.Last(pp => pp.Value).Position; // furthest target point from COM
                                start = COM + new VVector(1,1,1); // this will give a large penalty to these cases
                            }
                            else
                            {
                                start = segmentProfile2.First(p => p.Value).Position;
                                end = segmentProfile.Last(p => p.Value).Position;
                            }
                        }
                        else
                        {
                            end = segmentProfile1.Last(pp => pp.Value).Position;
                        }
                        isCovered = false;
                    }
                    else
                    {
                        end = segmentProfile1.Last(pp => pp.Value).Position;
                    }

                    if (target.IsPointInsideSegment(end)) // the isodose line is inside the structure => target is under-covered. CI value for this line will be negative
                    {
                        isCovered = false; 
                    }
                }
                // convex targets with external COM require consideration of a number of possible scenarios
                else if (segmentProfile.Any(p => p.Value) & segmentProfile1.Any(p => p.Value)) // if COM is outside target but the line dissects both structures
                {
                    start = segmentProfile.Last(pp => pp.Value).Position;
                    end = segmentProfile1.Last(pp => pp.Value).Position;
                }
                else if (!segmentProfile.Any(p => p.Value) & !segmentProfile1.Any(p => p.Value)) // if COM is outside target and there is no target or isodose contour in the searched direction
                {
                    // look in the opposite direction. The line will dissect target and isodose twice in this direction. Choose the closest dissection points this time.
                    VVector endPoint2 = COM - (endPoint - COM);
                    System.Collections.BitArray segmentStride2 = new System.Collections.BitArray(1000); // 0.1mm resolution?
                    SegmentProfile segmentProfile2 = target.GetSegmentProfile(COM, endPoint2, segmentStride2);
                    System.Collections.BitArray segmentStride3 = new System.Collections.BitArray(1000); // 0.1mm resolution?
                    SegmentProfile segmentProfile3 = refIsodose.GetSegmentProfile(COM, endPoint2, segmentStride3);

                    end = segmentProfile2.First(p => p.Value).Position;
                    start = segmentProfile3.First(p => p.Value).Position;

                    if (GetDistance3D(COM, start) > GetDistance3D(COM, end)) // If distance to isodose in this case is greater that means that the target is under-covered
                    {
                        isCovered = false;
                    }
                }
                else if (!segmentProfile.Any(p => p.Value) & refIsodose.IsPointInsideSegment(COM)) // concave target and convex isodose
                {
                    VVector endPoint2 = COM - (endPoint - COM);
                    System.Collections.BitArray segmentStride2 = new System.Collections.BitArray(1000); // 0.1mm resolution?
                    SegmentProfile segmentProfile2 = target.GetSegmentProfile(COM, endPoint2, segmentStride2);

                    start = segmentProfile2.First(p => p.Value).Position;
                    end = segmentProfile1.Last(p => p.Value).Position;
                }
                else if (!segmentProfile.Any(p => p.Value)) // COM outside target. Line dissects isodose but not target.
                {
                    start = segmentProfile1.First(p => p.Value).Position;
                    end = segmentProfile1.Last(p => p.Value).Position;
                }
                else if (!segmentProfile1.Any(p => p.Value)) // COM outside target. Line dissects target but not isodose. Target is under-covered
                {
                    start = segmentProfile.Last(p => p.Value).Position;
                    end = segmentProfile.First(p => p.Value).Position;
                    isCovered = false;
                }

                distances.Add(new Tuple<VVector, VVector>(start, end));
            }

            double sumDist = 0;

            foreach (var vpair in distances)
            {
                double dT = GetDistance3D(COM, vpair.Item1);
                double dD = GetDistance3D(COM, vpair.Item2);

                sumDist += ((dD - dT) / dT);
            }

            if (isCovered)
            {
                return sumDist / distances.Count() * 10;
            }
            else
            {
                return -1 * Math.Abs((sumDist / distances.Count()) * 10);
            }
        }

        // Wu et al 2003
        public static double CI_CDI(Structure target, Structure refIsodose)
        {
            double V1 = target.Volume;
            double V2 = refIsodose.Volume;
            double S1 = GetSurfaceArea(target);
            double S2 = GetSurfaceArea(refIsodose);

            double dist = (V2 - V1) / (0.5 * (S2 + S1));
            return dist;
        }


        // Helper methods

        public static double GetSurfaceArea(Structure structure)
        {
            var points = structure.MeshGeometry.Positions;
            var triangleIndices = structure.MeshGeometry.TriangleIndices;
            double area = 0;

            for (int i = 0; i < triangleIndices.Count() / 3; i++)
            {
                List<Point3D> triPoints = new List<Point3D>();
                Point3D a = points.ElementAt(triangleIndices.ElementAt(i * 3));
                Point3D b = points.ElementAt(triangleIndices.ElementAt((i * 3) + 1));
                Point3D c = points.ElementAt(triangleIndices.ElementAt((i * 3) + 2));

                Vector3D ab = new Vector3D(c.X - a.X, c.Y - a.Y, c.Z - a.Z);
                Vector3D ac = new Vector3D(b.X - a.X, b.Y - a.Y, b.Z - a.Z);

                double abDOTac = (ab.X * ac.X) + (ab.Y * ac.Y) + (ab.Z * ac.Z);

                if (ab.Length > 0 && ac.Length > 0)
                {
                    var val = 0.5 * ab.Length * ac.Length * Math.Sqrt(1 - Math.Pow(abDOTac / (ab.Length * ac.Length), 2));
                    if (!double.IsNaN(val))
                        area += val;
                }
            }
            return area / 100;
        }
        private static double GetDistance3D(VVector v1, VVector v2)
        {
            return Math.Sqrt(Math.Pow(v1.x - v2.x, 2) + Math.Pow(v1.y - v2.y, 2) + Math.Pow(v1.z - v2.z, 2));
        }

        private static double GetDistance3D(System.Windows.Media.Media3D.Point3D v1, System.Windows.Media.Media3D.Point3D v2)
        {
            return Math.Sqrt(Math.Pow(v1.X - v2.X, 2) + Math.Pow(v1.Y - v2.Y, 2) + Math.Pow(v1.Z - v2.Z, 2));
        }

        private static VVector Point3DToVVector(System.Windows.Media.Media3D.Point3D point)
        {
            return new VVector(point.X, point.Y, point.Z);
        }

        private static VVector Point3DToVVector(System.Windows.Media.Media3D.Vector3D point)
        {
            return new VVector(point.X, point.Y, point.Z);
        }

        private static System.Windows.Media.Media3D.Point3D VVectorToPoint3D(VVector point)
        {
            return new System.Windows.Media.Media3D.Point3D(point.x, point.y, point.z);
        }

        private static List<VVector> CreateSpherePoints(VVector COM, double R)
        {
            // Start with icosahedron
            List<VVector> icoPoints = new List<VVector> { new VVector(COM.x, COM.y, COM.z + R) };

            List<double> thetas = new List<double> { 2 * Math.PI / 360 * (90 - 26.565), 2 * Math.PI / 360 * (90 + 26.565) };
            List<double> phis = new List<double> { 0, 2 * Math.PI / 360 * 72, 2 * 2 * Math.PI / 360 * 72, 3 * 2 * Math.PI / 360 * 72, 4 * 2 * Math.PI / 360 * 72 };

            foreach (var ph in phis)
            {
                icoPoints.Add(new VVector(COM.x + R * Math.Sin(thetas[0]) * Math.Cos(ph), COM.y + R * Math.Sin(thetas[0]) * Math.Sin(ph), COM.z + R * Math.Cos(thetas[0])));
                icoPoints.Add(new VVector(COM.x + R * Math.Sin(thetas[1]) * Math.Cos(ph - (2 * Math.PI / 360 * 72.0 / 2.0)), COM.y + R * Math.Sin(thetas[1]) * Math.Sin(ph - (2 * Math.PI / 360 * 72.0 / 2.0)), COM.z + R * Math.Cos(thetas[1])));
            }


            icoPoints.Add(new VVector(COM.x, COM.y, COM.z - R));

            // Get midpoints

            var points1 = GetMidpoints(icoPoints, icoPoints);

            // Get midpoints again

            var points2 = GetMidpoints(points1, icoPoints);

            // Apparently at some point this leads to too many points for the program to handle. Gotta be something about memory available for ESAPI at runtime...

            // normalize to lie on sphere shell and return
            List<VVector> spherePoints = new List<VVector>();

            foreach (var p in points2)
            {
                spherePoints.Add(NormalizeToSphere(p, COM, R));
            }

            return spherePoints;
        }

        // Move point p along |p COM| line to lie at radius R from COM
        private static VVector NormalizeToSphere(VVector origPoint, VVector COM, double R)
        {
            double L = R / (origPoint - COM).Length;
            return new VVector(COM.x + L * (origPoint.x - COM.x), COM.y + L * (origPoint.y - COM.y), COM.z + L * (origPoint.z - COM.z));
        }

        private static List<VVector> GetMidpoints(List<VVector> origPoints, List<VVector> pentPoints)
        {
            // List to save evaluated point pairs to avoid double evaluation of lines
            List<Tuple<VVector, VVector>> evaluatedPairs = new List<Tuple<VVector, VVector>>();

            // List of return values
            List<VVector> points = new List<VVector>();
            points.AddRange(origPoints);

            foreach (var point in origPoints)
            {
                // find nearest neighbors
                var NN = GetNN(point, origPoints, pentPoints);

                foreach (var nb in NN)
                {
                    // avoid double evaluations
                    if (evaluatedPairs.Contains(new Tuple<VVector, VVector>(point, nb)) || evaluatedPairs.Contains(new Tuple<VVector, VVector>(nb, point)))
                    {
                        continue;
                    }
                    else
                    {
                        VVector AB = nb - point;
                        points.Add(point + (AB / AB.Length * AB.Length / 2.0));

                        evaluatedPairs.Add(new Tuple<VVector, VVector>(point, nb));
                    }
                }
            }

            return points;
        }

        private static List<VVector> GetNN(VVector point, List<VVector> points, List<VVector> pentPoints)
        {
            // Calculate distances for all points
            List<double> dist = new List<double>();

            for (int i = 0; i < points.Count(); i++)
            {
                dist.Add(GetDistance3D(point, points.ElementAt(i)));
            }

            // sort distances in ascending order
            var sorted = dist
                .Select((x, i) => new KeyValuePair<double, int>(x, i))
                .OrderBy(x => x.Key)
                .ToList();

            List<double> sDist = sorted.Select(x => x.Key).ToList();
            List<int> idx = sorted.Select(x => x.Value).ToList();

            // return five or six nearest neighbors, depending whether point is center of a pentagram (original icosahedron vertex) or not
            if (pentPoints.Contains(point))
            {
                return new List<VVector> { points[idx[1]], points[idx[2]], points[idx[3]], points[idx[4]], points[idx[5]] }; // index 0 will be point's distance to itself
            }
            else
            {
                return new List<VVector> { points[idx[1]], points[idx[2]], points[idx[3]], points[idx[4]], points[idx[5]], points[idx[6]] }; // index 0 will be point's distance to itself
            }
        }
    }
}
