﻿// This implementation of the 3D connected component labeling algorithm finds volumes with a specific dose level. 
// Adapted from 2D implementation: https://www.codeproject.com/Articles/336915/Connected-Component-Labeling-Algorithm


using System.Collections.Generic;
using System.Drawing;
using System.ComponentModel.Composition;
using System.Linq;

namespace ConnectedComponentLabeling
{
    [Export(typeof(IConnectedComponentLabeling))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class HotCCL : IConnectedComponentLabeling
    {
        #region Member Variables

        private int[,,] _board;
        private double[,,] _input;
        private int _width;
        private int _height;
        private int _depth;
        private double _cutoff;

        #endregion

        #region IConnectedComponentLabeling
        // Implement the Process method from IConnectedComponentLabeling

        public IDictionary<int, double[,,]> Process(double[,,] input, double cutoff)
        {
            // initialize values
            _input = input;
            _width = input.GetLength(0);
            _height = input.GetLength(1);
            _depth = input.GetLength(2);
            _board = new int[_width, _height, _depth]; // Each voxel's label will be saved here
            _cutoff = cutoff;

            Dictionary<int, List<Pixel>> patterns = Find();
            var images = new Dictionary<int, double[,,]>();

            foreach (KeyValuePair<int, List<Pixel>> pattern in patterns)
            {
                double[,,] blob = Create3DImage(pattern.Value);
                images.Add(pattern.Key, blob);
            }

            return images;
        }

        public IDictionary<int, List<Pixel>> ProcessReturnPixels(double[,,] input, double cutoff)
        {
            _input = input;
            _width = input.GetLength(0);
            _height = input.GetLength(1);
            _depth = input.GetLength(2);
            _board = new int[_width, _height, _depth];
            _cutoff = cutoff;

            Dictionary<int, List<Pixel>> patterns = Find();

            return patterns;
        }

        public IDictionary<int, int> ProcessReturnOnlySize(double[,,] input, double cutoff)
        {
            // initialize values
            _input = input;
            _width = input.GetLength(0);
            _height = input.GetLength(1);
            _depth = input.GetLength(2);
            _board = new int[_width, _height, _depth]; // Each voxel's label will be saved here
            _cutoff = cutoff;

            Dictionary<int, List<Pixel>> patterns = Find();
            var images = new Dictionary<int, int>();

            foreach (KeyValuePair<int, List<Pixel>> pattern in patterns)
            {
                int size = GetSize(pattern.Value);
                images.Add(pattern.Key, size);
            }

            return images;
        }

        #endregion

        #region Protected Methods

        protected virtual bool CheckIsBackGround(Pixel currentPixel)
        {
            return (currentPixel.dose > _cutoff); // doses should be binned in 0.1 Gy steps
        }

        #endregion

        #region Private Methods

        private Dictionary<int, List<Pixel>> Find()
        {
            // start at label nr 1
            int labelCount = 1;
            var allLabels = new Dictionary<int, Label>();

            for (int i = 0; i < _height; i++)
            {
                for (int j = 0; j < _width; j++)
                {
                    for (int k = 0; k < _depth; k++)
                    {
                        Pixel currentPixel = new Pixel(new VectorThreeD(j, i, k), _input[j, i, k]); // current voxel location and dose value

                        if (CheckIsBackGround(currentPixel))
                        {
                            continue; // do nothing if the voxel is background
                        }

                        // get a list of the neighboring voxels' labels
                        IEnumerable<int> neighboringLabels = GetNeighboringLabels(currentPixel);
                        int currentLabel;

                        // if neighboring voxels are not labeled, assign new label to current voxel and increase labelCount
                        if (!neighboringLabels.Any())
                        {
                            currentLabel = labelCount;
                            allLabels.Add(currentLabel, new Label(currentLabel));
                            labelCount++;
                        }
                        // if neighboring voxels are labeled, assign smallest neighbor label to voxel
                        else
                        {
                            currentLabel = neighboringLabels.Min(n => allLabels[n].GetRoot().Name); // gets the root of the neighbor voxel with the smallest label
                            Label root = allLabels[currentLabel].GetRoot(); // get the root of the current voxel

                            foreach (var neighbor in neighboringLabels)
                            {
                                // if neighbor roots are not equal to current root, fix this
                                if (root.Name != allLabels[neighbor].GetRoot().Name)
                                {
                                    allLabels[neighbor].Join(allLabels[currentLabel]);
                                }
                            }
                        }
                        // assign label and save to board array
                        _board[j, i, k] = currentLabel;
                    }
                }
            }


            Dictionary<int, List<Pixel>> patterns = AggregatePatterns(allLabels);

            return patterns;

        }

        private IEnumerable<int> GetNeighboringLabels(Pixel pix)
        {
            var neighboringLabels = new List<int>();

            // Search Voxel neighborhood and add neighboring labels to list
            for (int i = pix.Position.Y - 1; i <= pix.Position.Y + 2 && i < _height - 1; i++) // why go out to +2? Is there an issue here?
            {
                for (int j = pix.Position.X - 1; j <= pix.Position.X + 2 && j < _width - 1; j++)
                {
                    for (int k = pix.Position.Z - 1; k <= pix.Position.Z + 2 && k < _depth - 1; k++)
                    {
                        if (i > -1 && j > -1 && k > -1 && _board[j, i, k] != 0) // if the neighbor voxel has a label
                        {
                            neighboringLabels.Add(_board[j, i, k]);
                        }
                    }
                }
            }

            return neighboringLabels;
        }

        private Dictionary<int, List<Pixel>> AggregatePatterns(Dictionary<int, Label> allLabels)
        {
            var patterns = new Dictionary<int, List<Pixel>>();

            for (int i = 0; i < _height; i++)
            {
                for (int j = 0; j < _width; j++)
                {
                    for (int k = 0; k < _depth; k++)
                    {
                        int patternNumber = _board[j, i, k];

                        if (patternNumber != 0)
                        {
                            patternNumber = allLabels[patternNumber].GetRoot().Name;

                            if (!patterns.ContainsKey(patternNumber))
                            {
                                patterns[patternNumber] = new List<Pixel>();
                            }

                            patterns[patternNumber].Add(new Pixel(new VectorThreeD(j, i, k), 999)); // Change so that each pixel is given its actual dose?
                        }
                    }
                }
            }

            return patterns;
        }

        private double[,,] Create3DImage(List<Pixel> pattern)
        {
            int minX = pattern.Min(p => p.Position.X);
            int maxX = pattern.Max(p => p.Position.X);

            int minY = pattern.Min(p => p.Position.Y);
            int maxY = pattern.Max(p => p.Position.Y);

            int minZ = pattern.Min(p => p.Position.Z);
            int maxZ = pattern.Max(p => p.Position.Z);

            int width = maxX + 1 - minX;
            int height = maxY + 1 - minY;
            int depth = maxZ + 1 - minZ;

            var blob = new double[width, height, depth];

            foreach (Pixel pix in pattern)
            {
                blob.SetValue(pix.dose, pix.Position.X - minX, pix.Position.Y - minY, pix.Position.Z - minZ);//shift position by minX, minY and minZ
            }

            return blob;
        }

        private int GetSize(List<Pixel> pattern)
        {
            int size = 0;

            foreach (Pixel p in pattern)
            {
                if (p.dose > 0) // if voxel is not background
                {
                    size++; // add one voxel to size
                }
            }

            return size;
        }

        #endregion
    }
}
